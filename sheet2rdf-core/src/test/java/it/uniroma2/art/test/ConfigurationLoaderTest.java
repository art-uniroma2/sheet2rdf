package it.uniroma2.art.test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.uniroma2.art.sheet2rdf.cfg.GraphApplicationConfigurationLoader;
import it.uniroma2.art.sheet2rdf.cfg.GraphApplicationConfigurationParser;
import it.uniroma2.art.sheet2rdf.header.AdvancedGraphApplication;
import it.uniroma2.art.sheet2rdf.header.NodeConversion;

public class ConfigurationLoaderTest {
	
	public static void main(String[] args) throws IOException {
		
		File cfgFile = GraphApplicationConfigurationLoader.getConfigurationFile(GraphApplicationConfigurationLoader.XLABEL_CONFIGURATION_FILE);
		ObjectNode cfgJson = GraphApplicationConfigurationLoader.loadConfigurationFromYAMLFiles(cfgFile);
		System.out.println(cfgJson);
		
		GraphApplicationConfigurationParser parser = new GraphApplicationConfigurationParser(cfgJson, "prefix");
		List<NodeConversion> nodes = parser.getNodeConversions();
		AdvancedGraphApplication g = parser.getAdvancedGraphApplication(nodes, null);
		
		System.out.println("g " + g);
		System.out.println("\nnodes " + nodes);
		
	}
	
}
