package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SheetManagerJUnit {
	
	boolean debug = true;
	
	private static final int expectedColumns = 9;
	private static final int expectedRows = 30;
	private static final int rowIndexTest = 2;
	private static final int colIndexTest = 2;
	private static final String expectedCellContent = "c_2018";
	//for xlsx, expected headers are different (probably due to changes in the past)
	private static final String expectedHeadersXlsx = "[skos:Concept, skos:narrower, skos:narrower, skos:narrower, " +
			"rdfs:label^^xsd:string, skos:narrower, skos:prefLabel@en, skos:prefLabel@it, skosxl:prefLabel@en]";
	private static final String expectedHeaders = "[skos:Concept, skos:narrower, skos:narrower, skos:narrower, " +
			"skos:narrower, skos:narrower, skos:prefLabel@en, skos:prefLabel@it, skosxl:prefLabel@en]";
	
	@Test
	public void testXlsx() throws GenericSheet2RDFException, NotExistingSheetException {
		if (debug)
			System.out.println("\nXLSX test");

		String filePath = "S2RDFfiles/AgrovocSample.xlsx";
		File file = new File(SheetAnnotatorTest.class.getClassLoader().getResource(filePath).getPath());

		SpreadsheetManager spreadsheetManager = SpreadsheetManagerFactory.getSpreadsheetManager(file);
		S2RDFSheet sheet = spreadsheetManager.getSheet(0);

		if (debug) {
			System.out.println("Datasheet columns: expected " + expectedColumns + ", returned "
					+ sheet.getColumnCount());
		}
		assertEquals(sheet.getColumnCount(), expectedColumns);
		if (debug) {
			System.out.println("Datasheet rows: expected " + expectedRows + ", returned "
					+ sheet.getRowCount());
		}
		assertEquals(sheet.getRowCount(), expectedRows);
		if (debug) {
			System.out.println("Cella (" + rowIndexTest + "," + colIndexTest + "): expected '" + expectedCellContent
					+ "', returned '" + sheet.getCellValue(rowIndexTest, colIndexTest) + "'");
		}
		assertEquals(sheet.getCellValue(rowIndexTest, colIndexTest), expectedCellContent);

		List<String> headers = sheet.getHeaders();
		if (debug) {
			System.out.println("Headers: expected\n" + expectedHeadersXlsx + "\nreturned\n" + headers);
		}
		assertEquals(headers.toString(), expectedHeadersXlsx);
	}
	
	@Test
	public void testCsv() throws GenericSheet2RDFException, NotExistingSheetException {
		if (debug)
			System.out.println("\nCSV test");
		
		String filePath = "S2RDFfiles/AgrovocSample.csv";
		File file = new File(SheetAnnotatorTest.class.getClassLoader().getResource(filePath).getPath());

		SpreadsheetManager spreadsheetManager = SpreadsheetManagerFactory.getSpreadsheetManager(file);
		S2RDFSheet sheet = spreadsheetManager.getSheet(0);

		if (debug) {
			System.out.println("Datasheet columns: expected " + expectedColumns + ", returned " 
					+ sheet.getColumnCount());
		}
		assertEquals(sheet.getColumnCount(), expectedColumns);
		if (debug) {
			System.out.println("Datasheet rows: expected " + expectedRows + ", returned " 
					+ sheet.getRowCount());
		}
		assertEquals(sheet.getRowCount(), expectedRows);
		if (debug) {
			System.out.println("Cella ("+rowIndexTest+","+colIndexTest+"): expected '" + expectedCellContent 
					+ "', returned '" + sheet.getCellValue(rowIndexTest, colIndexTest) + "'");
		}
		assertEquals(sheet.getCellValue(rowIndexTest, colIndexTest), expectedCellContent);
		
		List<String> headers = sheet.getHeaders();
		if (debug) {
			System.out.println("Headers: expected\n" + expectedHeaders + "\nreturned\n" + headers);
		}
		assertEquals(headers.toString(), expectedHeaders);
	}
	
	@Test
	public void testOds() throws GenericSheet2RDFException, NotExistingSheetException {
		if (debug) 
			System.out.println("\nODS test");

		String filePath = "S2RDFfiles/AgrovocSample.ods";
		File file = new File(SheetAnnotatorTest.class.getClassLoader().getResource(filePath).getPath());

		SpreadsheetManager spreadsheetManager = SpreadsheetManagerFactory.getSpreadsheetManager(file);
		S2RDFSheet sheet = spreadsheetManager.getSheet(0);
		
		if (debug) {
			System.out.println("Datasheet columns: expected " + expectedColumns + ", returned " 
					+ sheet.getColumnCount());
		}
		assertEquals(sheet.getColumnCount(), expectedColumns);
		if (debug) {
			System.out.println("Datasheet rows: expected " + expectedRows + ", returned " 
					+ sheet.getRowCount());
		}
		assertEquals(sheet.getRowCount(), expectedRows);
		if (debug) {
			System.out.println("Cella ("+rowIndexTest+","+colIndexTest+"): expected '" + expectedCellContent 
					+ "', returned '" + sheet.getCellValue(rowIndexTest, colIndexTest) + "'");
		}
		assertEquals(sheet.getCellValue(rowIndexTest, colIndexTest), expectedCellContent);
		
		List<String> headers = sheet.getHeaders();
		if (debug) {
			System.out.println("Headers: expected\n" + expectedHeaders + "\nreturned\n" + headers);
		}
		assertEquals(headers.toString(), expectedHeaders);
	}

}
