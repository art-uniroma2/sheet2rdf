package it.uniroma2.art.test;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.sheet2rdf.coda.CODAConverter;
import it.uniroma2.art.sheet2rdf.resolver.ConverterResolver;
import it.uniroma2.art.sheet2rdf.resolver.PropertyRangeResolver;

public class PropertyConverterResolverTest extends AbstractTest {

	public static void main(String[] args) throws Exception {
		
		RepositoryConnection connection = getInitializedConnection();
		
		PropertyRangeResolver rangeResolver = new PropertyRangeResolver(connection);
		
		printConverter(SKOS.CONCEPT);
		printConverter(rangeResolver.getRange(SKOS.PREF_LABEL));

	}
	
	private static void printConverter(IRI cls) {
		System.out.println("Class: " + cls);
		CODAConverter converter = ConverterResolver.getConverter(cls);
		if (converter != null) {
			System.out.println("type:\t" + converter.getType());
			System.out.println("uri:\t" + converter.getContractUri());
			System.out.println("lang:\t" + converter.getLanguage());
			System.out.println("dt:\t" + converter.getDatatypeUri());
			System.out.println("param:\t" + converter.getParams());
		} else {
			System.out.println("null");
		}
		System.out.println();
	}

}
