package it.uniroma2.art.test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.utils.StatusHandler;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFParseException;

import it.uniroma2.art.sheet2rdf.core.MappingStruct;
import it.uniroma2.art.sheet2rdf.core.Sheet2RDFCore;
import it.uniroma2.art.sheet2rdf.exception.InvalidWizardStatusException;
import it.uniroma2.art.sheet2rdf.header.GraphApplication;
import it.uniroma2.art.sheet2rdf.header.NodeConversion;
import it.uniroma2.art.sheet2rdf.header.SimpleGraphApplication;
import it.uniroma2.art.sheet2rdf.header.SimpleHeader;
import it.uniroma2.art.sheet2rdf.header.SubjectHeader;

public class FromJsonTest extends AbstractTest {

	public static void main(String[] args) throws RDFParseException, RepositoryException, IOException, InvalidWizardStatusException, GenericSheet2RDFException {
		
		String xlsFilePath = "S2RDFfiles/test_status.xlsx";
		File excelFile = new File(HeadersTest.class.getClassLoader().getResource(xlsFilePath).getPath());

		String jsonFilePath = "S2RDFfiles/s2rdf_status.json";
		File jsonFileInput = new File(HeadersTest.class.getClassLoader().getResource(jsonFilePath).getPath());

		RepositoryConnection connection = getInitializedConnection();
		Sheet2RDFCore s2rdfCore = new Sheet2RDFCore(excelFile, connection);
		
		Map<String, MappingStruct> msMap = s2rdfCore.getMappingStructMap();
		MappingStruct ms = msMap.values().iterator().next();
		StatusHandler.restoreSheetStatus(jsonFileInput, ms, connection);
		
		System.out.println("FS header map\t" + ms.getHeaderFsMap() + "\n");
		
		{
			SubjectHeader subjHeader = ms.getSubjectHeader();
			System.out.println("SUBJECT HEADER");
			System.out.println("id:\t\t" + subjHeader.getId());
			NodeConversion nc = subjHeader.getNodeConversion();
			System.out.println("node - ");
			System.out.println("id:\t" + nc.getNodeId());
			System.out.println("memoize:\t" + nc.getMemoization().isEnabled());
			System.out.println("conv -----------\n" + nc.getConverter() + "\n---------------------");
			SimpleGraphApplication ga = subjHeader.getGraphApplication();
			System.out.println("graph - ");
			System.out.println(ga);
			System.out.println();
		}
		
		System.out.println("HEADERS");
		List<SimpleHeader> headers = ms.getHeaders();
		for (SimpleHeader h : headers) {
			System.out.println("id:\t\t" + h.getId());
			System.out.println("nameStruct:\t" + h.getHeaderNameStruct());
			System.out.println("ignore:\t\t" + h.isIgnore());
			
			System.out.println("nodes - ");
			for (NodeConversion nc : h.getNodeConversions()) {
				System.out.println("id:\t\t" + nc.getNodeId());
				System.out.println("memoize:\t" + nc.getMemoization().isEnabled());
				System.out.println("conv -----------\n" + nc.getConverter() + "\n---------------------");
			}
			
			System.out.println("graphs - ");
			for (GraphApplication ga : h.getGraphApplications()) {
				System.out.println(ga);
			}
			System.out.println();
		}
		
	}

}
