package it.uniroma2.art.test;

import java.io.File;
import java.util.List;

import com.google.common.io.Files;
import it.uniroma2.art.coda.pf4j.PF4JComponentProvider;
import it.uniroma2.art.coda.structures.CODATriple;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import it.uniroma2.art.sheet2rdf.coda.Sheet2RDFCODA;
import it.uniroma2.art.sheet2rdf.core.Sheet2RDFCore;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;

public class S2RDFCODATest extends AbstractTest {

	public static void main(String[] args) throws Exception {

		//Ontology model
		RepositoryConnection connection = getInitializedConnection();
		
		//Excel2RDF parameters
		String xlsFilePath = "S2RDFfiles/AgrovocSample.xlsx";
		File excelFile = new File(S2RDFCODATest.class.getClassLoader().getResource(xlsFilePath).getPath());

		File pearlFile = File.createTempFile("pearl", ".pr");

		PluginManager pluginManager = new DefaultPluginManager();
		pluginManager.loadPlugins();
		pluginManager.startPlugins();

		PF4JComponentProvider cp = new PF4JComponentProvider(pluginManager);
		CODACore codaCore = new CODACore(cp);

		Sheet2RDFCODA s2rdfCoda = new Sheet2RDFCODA(connection, codaCore);
		Sheet2RDFCore s2rdfCore = new Sheet2RDFCore(excelFile, connection);
		

		try {
			String firstSheetName = s2rdfCore.getSheetNames().get(0);
			JCas jcas = s2rdfCore.executeAnnotator(firstSheetName);
//			analysisCAS(jcas);
			List<SuggOntologyCoda> listSuggOnt = s2rdfCoda.suggestTriples(jcas, pearlFile);

			int i = 0;
			for (SuggOntologyCoda sugg : listSuggOnt){
				List<CODATriple> triples = sugg.getAllInsertARTTriple();
				System.out.println("\nAnnotation type: " + sugg.getAnnotation().getType().getName()
						+ "\nbegin/end " + sugg.getAnnotation().getBegin()
						+ "/" + sugg.getAnnotation().getEnd());
				for (CODATriple t : triples){
					System.out.println(i+") Tripla:");
					System.out.println("\t" + NTriplesUtil.toNTriplesString(t.getSubject()));
					System.out.println("\t" + NTriplesUtil.toNTriplesString(t.getPredicate()));
					System.out.println("\t" + NTriplesUtil.toNTriplesString(t.getObject()));
					i++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			codaCore.stopAndClose();
		}
		
	}
	
	public static void analysisCAS(JCas jcas) {
		FSIterator<Annotation> iter = jcas.getAnnotationIndex().iterator();
		while (iter.hasNext()) {
			System.out.println("****************************************");
			Annotation ann = iter.next();
			analysisFeatureStruct(ann, 0);
		}
	}

	public static void analysisFeatureStruct(FeatureStructure fs, int indent) {
		String tabs = "";
		for (int i = 0; i < indent; ++i) {
			tabs += "\t";
		}
		System.out.println(tabs + "type = " + fs.getType().getShortName());
		if (fs instanceof AnnotationFS) {
			String coveredText = ((AnnotationFS) fs).getCoveredText();
			if (coveredText.length() < 64)
				System.out.println(tabs + "coveredText = " + coveredText);
			else
				System.out.println(tabs + "coveredText = " + coveredText.substring(0, 64) + "...");
		}
		List<Feature> featList = fs.getType().getFeatures();
		for (Feature feature : featList) {
			String rangeType = feature.getRange().getName();
			// System.out.println(tabs+"rangeType == "+rangeType);
			if (CAS.TYPE_NAME_STRING.equals(rangeType)) {
				String value = fs.getStringValue(feature);
				System.out.println(tabs + feature.getShortName() + " -> " + value);
			} else if (CAS.TYPE_NAME_INTEGER.equals(rangeType)) {
				int value = fs.getIntValue(feature);
				System.out.println(tabs + feature.getShortName() + " -> " + value);
			} else if (CAS.TYPE_NAME_STRING_ARRAY.equals(rangeType)) {
				StringArray sa = (StringArray) fs.getFeatureValue(feature);
				if (sa == null)
					continue;
				String[] array = sa.toArray();
				String value = "[";
				for (int i = 0; i < array.length; ++i) {
					value += array[i];
					if (i != array.length - 1)
						value += ", ";
				}
				value += "]";
				System.out.println(tabs + feature.getShortName() + " -> " + value);
			} else if (feature.getRange().isArray()) {
				FSArray fsa = (FSArray) fs.getFeatureValue(feature);
				if (fsa == null)
					continue;
				FeatureStructure[] array = fsa.toArray();
				System.out.println(tabs + feature.getShortName() + " ---> ");
				for (FeatureStructure featureStructure : array) {
					analysisFeatureStruct(featureStructure, indent + 1);
				}
			} else {
				System.out.println(tabs + feature.getShortName() + " --> ");
				FeatureStructure value = fs.getFeatureValue(feature);
				if (value != null)
					analysisFeatureStruct(value, indent + 1);
			}
		}
	}

}
