package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;

import java.io.File;
import java.util.List;
import java.util.Map;

public class SheetManagerTest {
	
	public static void main(String[] args) throws GenericSheet2RDFException, NotExistingSheetException {

		String filePath = "S2RDFfiles/AgrovocSample.ods";
		File file = new File(SheetManagerTest.class.getClassLoader().getResource(filePath).getPath());

		SpreadsheetManager spreadsheetMgr = SpreadsheetManagerFactory.getSpreadsheetManager(file);

		Map<String, String> map = spreadsheetMgr.getPrefixNamespaceMapping();
		if (map.isEmpty()) {
			System.out.println("Empty prefix ns mapping");
		} else {
			System.out.println("Prefix ns mappings:");
			for (String p : map.keySet()) {
				System.out.println(p + " - " + map.get(p));
			}
		}

		for (S2RDFSheet sheets : spreadsheetMgr.getSheets()) {

			System.out.println("SHEET: " + sheets.getName());

			System.out.println("colonne:\t" + sheets.getColumnCount());
			System.out.println("righe:\t" + sheets.getRowCount());

			int r = 0;
			int c = 0;
			System.out.print("cella (" + r + "," + c + "): ");
			System.out.println(sheets.getCellValue(r, c));

			List<String> headers = sheets.getHeaders();
			for (String h : headers) {
				System.out.print(h + "\t");
			}
			System.out.println();

			System.out.println("Table content:");
			List<List<String>> table = sheets.getTableContent();
			for (r = 0; r < table.size(); r++) {
				for (c = 0; c < table.get(0).size(); c++) {
					System.out.print(table.get(r).get(c) + "\t");
				}
				System.out.println();
			}
		}
	}

}
