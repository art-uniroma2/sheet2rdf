package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.core.SpreadsheetOrDb;
import it.uniroma2.art.sheet2rdf.db.AvailableDBDriverName;
import it.uniroma2.art.sheet2rdf.db.DBInfo;
import it.uniroma2.art.sheet2rdf.exception.DBLocalizedException;
import it.uniroma2.art.sheet2rdf.sheet.db.DBTableManager;

import java.util.ArrayList;
import java.util.List;

public class TestDB_toFill {

    AvailableDBDriverName.Values DB_TYPE;
    String DB_URL_PORT = "";
    String USER = "";
    String PASS = "";
    String DB_NAME = "";
    String DB_TABLE = "";


    public static void main(String[] args) throws DBLocalizedException {
        TestDB_toFill testDBToDel = new TestDB_toFill();

        testDBToDel.doTest();
    }

    private void doTest() throws DBLocalizedException {

        List<String> tableList = new ArrayList<>();
        tableList.add(DB_TABLE);
        DBInfo dbInfo = new DBInfo(DB_URL_PORT, DB_NAME, tableList, USER, PASS, DB_TYPE);
        SpreadsheetOrDb spreadsheetOrDb = new SpreadsheetOrDb(dbInfo);

        DBTableManager dbTableManager = new DBTableManager(spreadsheetOrDb, DB_TABLE);

        //get the headers
        List<String> headerList = dbTableManager.getHeaders(true);
        System.out.println("headerList:\n"+headerList+"\n");

        //get the values of the table(s)
        List<List<String>> valueFromTable = dbTableManager.getDataTable();
        System.out.println("valueFromTable:\n:"+valueFromTable+"\n");

    }
}
