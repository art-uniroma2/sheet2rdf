package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.resolver.PropertyRangeResolver;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;

public class PropertyRangeResolverFromOntologyTest extends AbstractTest {

	public static void main(String[] args) throws Exception{
		
		RepositoryConnection connection = getInitializedConnection();


		
		PropertyRangeResolver resolver = new PropertyRangeResolver(connection);

		RepositoryResult<Statement> propStmts = connection.getStatements(null, RDF.TYPE, RDF.PROPERTY);
		//for each property print the range found with the resolver
		while (propStmts.hasNext()){
			IRI prop = (IRI) propStmts.next().getSubject();
			Resource range = resolver.getRange(prop);
			System.out.println("\nprop:\t"+prop);
			System.out.println("range with resolver:\t" + range);
		}

		//test for checking if loop of props (without ranges) causes error
		SimpleValueFactory vf = SimpleValueFactory.getInstance();
		IRI prop1 = vf.createIRI("http://temp#loop1");
		IRI prop2 = vf.createIRI("http://temp#loop2");
		IRI prop3 = vf.createIRI("http://temp#loop3");
		connection.add(prop3, RDFS.SUBPROPERTYOF, prop2);
		connection.add(prop2, RDFS.SUBPROPERTYOF, prop1);
		connection.add(prop1, RDFS.SUBPROPERTYOF, prop3);
		System.out.println("range prop1: " + resolver.getRange(prop1));
		
	}

}
