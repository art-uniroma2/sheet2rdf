package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.cli.Sheet2RDF_CLI;

public class S2RDF_CliTest {

	public static void main(String[] args) throws Exception {

		String filePath = "./src/test/resources/S2RDFfiles/s2rdfProperties.properties";
		String[] arg = {filePath};
		Sheet2RDF_CLI.main(arg);
	}

}
