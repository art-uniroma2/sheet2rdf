package it.uniroma2.art.test;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.util.ArrayList;

/**
 * Class that performs some checks on the Excel file to verify that it is compliant with some standards.
 * At the end of the execution of this class, it reports eventual warnings contained in the Excel sheets.
 * 
 * Standards:
 * 
 * The first sheet must contains a table with headers and data.
 * The headers fill the first row and starts from first column.
 * There should not be empty cell in the headers row.
 * First column could be "<subject>". In this case there should be no empty cells under this column. 
 * Every headers should not start or end with blank spaces, eg. " header ".
 * 
 * Eventually there should be a second sheet named "prefix_mapping" structured as follow:
 * -the first column contains a prefix. Every prefix must not have blank spaces at the end.
 * -the second
 * There should be no empty cells under the first (prefix) and second (namespace) columns.
 * If prefix_mapping sheet is defined (and filled), then every header (eventually except first if it is the
 * <subject> column) must start with a prefix followed by ":".
 * 
 * @author Tiziano Lorenzetti
 *
 */
public class StandardsCompliantChecker {
	
	public static void main(String[] args) throws Exception{
		
		boolean warning = false;
		Sheet dataSheet;
		int nDataSheetColumn;
		int nDataSheetRow;
		Sheet prefSheet;
		int nPrefSheetRow;

		String xlsFilePath = "S2RDFfiles/AgrovocSample.xlsx";
		File excelFile = new File(StandardsCompliantChecker.class.getClassLoader().getResource(xlsFilePath).getPath());
		
		Workbook workbook = WorkbookFactory.create(excelFile);
		
		dataSheet = workbook.getSheetAt(0);
		
		//-----CHECKS ON THE DATA SHEET-----
		
		Row headerRow = dataSheet.getRow(0);
		/* check if there is data into the first sheet (NB: if there's only headers row is not a problem,
		 * in fact ExcelAnnotator will not populate any feature. */
		if (headerRow == null) {
			System.out.println("ERROR (data sheet): no data found in the first sheet ("+workbook.getSheetName(0)+")");
			return;
		}
		nDataSheetColumn = headerRow.getLastCellNum();
		
		//check if there is an empty cell in the headers row
		for (int i = 0; i < nDataSheetColumn; i++){
			Cell hCell = headerRow.getCell(i);
			if (hCell == null){
				System.out.println("ERROR (data sheet): empty cell found in the header at column n. " + (i+1) 
						+ ". Please make sure that there are no empty cell in the headers row.");
				return;
			}
		}
		
		//check if there are no empty rows in data sheet
		nDataSheetRow = dataSheet.getLastRowNum();
		for (int i = 0; i <= nDataSheetRow; i++){
			Row row = dataSheet.getRow(i);
			if (row == null){
				System.out.println("ERROR (data sheet): empty row found at line n. " + (i + 1)
						+ ". Please, make sure that there are no empty rows.");
				return;
			}
		}
		
		//check if there is any header that starts or ends with blank spaces
		for (int i = 0; i < nDataSheetColumn; i++) {
			String h = headerRow.getCell(i).getStringCellValue();
			if (h.startsWith(" ") || h.endsWith(" ")) {
				System.out.println("WARNING (data sheet): blank space(s) found in column n. " + (i + 1)
						+ " (header: \"" + h + "\"). Please remove the space(s)");
				warning = true;
			}
		}
		
		//if a <subject> column is defined, check if every subject is specified
		boolean subjColumn = (headerRow.getCell(0).getStringCellValue().trim()).equals("<subject>");
		int firstHeaderColumn = 0;
		if (subjColumn) {
			firstHeaderColumn = 1;
			for (int i = 1; i <= nDataSheetRow; i++){
				Cell subjCell = dataSheet.getRow(i).getCell(0);
				if (subjCell == null){
					System.out.println("ERROR (data sheet): subject at row n. " + (i+1) + " not specified.");
					return;
				}
			}
		}
		
		//-----CHECKS PERFORMED ONLY IF A PREFIX_MAPPING SHEET IS DEFINED AND FILLED-----
		
		prefSheet = workbook.getSheet("prefix_mapping");

		if (prefSheet != null && prefSheet.getPhysicalNumberOfRows() != 0){
			//check if there are no empty rows in prefix_mapping sheet
			nPrefSheetRow = prefSheet.getLastRowNum();
			for (int i = 0; i <= nPrefSheetRow; i++){
				Row row = prefSheet.getRow(i);
				if (row == null){
					System.out.println("ERROR (prefix_mapping sheet): empty row found at line n. " + (i + 1)
							+ ". Please, make sure that there are no empty rows.");
					return;
				}
			}
			
			//check if every row of prefix_mapping sheet is filled with both prefix and namespace
			//and if there is any prefix that starts or ends with blank space(s)
			for (int i = 0; i <= nPrefSheetRow; i++){
				Cell prefCell = prefSheet.getRow(i).getCell(0);
				Cell nsCell = prefSheet.getRow(i).getCell(1);
				if (prefCell == null || nsCell == null){
					System.out
							.println("ERROR (prefix_mapping sheet): row n. " + (i + 1)
									+ " not filled correctly. Please check if both prefix and namespace are defined.");
					return;
				}
				String pref = prefCell.getStringCellValue();
				if (pref.startsWith(" ") || pref.endsWith(" ")){
					System.out.println("WARNING (prefix_mapping sheet): prefix at row n. " + (i + 1)
							+ " (pref: \"" + pref
							+ "\") starts or ends with blank space(s). Please, remove the space(s).");
					warning = true;
				}
			}
			//check if every header in the data sheet starts with a prefix defined in prefix_mapping sheet
			//collect prefixes
			ArrayList<String> prefixes = new ArrayList<>();
			for (int i = 0; i <= nPrefSheetRow; i++){
				prefixes.add(prefSheet.getRow(i).getCell(0).getStringCellValue());
			}
			//check headers
			for (int i = firstHeaderColumn; i < nDataSheetColumn; i++){
				boolean ok = false;
				String h = headerRow.getCell(i).getStringCellValue();
				for (String p : prefixes){
					if (h.startsWith(p+":")){
						ok = true;
						break;
					}
				}
				if (!ok){
					System.out.println("WARNING (data sheet): header in column n. " + (i + 1)	+ " (\"" + h
							+ "\") is not a valid qname (it must starts with a prefix defined in prefix_sheet)");
					warning = true;
				}
			}
		}
		
		if (!warning) 
			System.out.println("No problems found.");
	}

}
