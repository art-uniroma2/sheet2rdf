package it.uniroma2.art.test;

import java.util.List;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.IntegerArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.jcas.tcas.Annotation;

public class UIMAUtils {

	static public String analysisCAS(JCas jcas) {

		StringBuilder sb = new StringBuilder();
		
		FSIterator<Annotation> iter = jcas.getAnnotationIndex().iterator();

		while (iter.hasNext()) {
			sb.append("\n****************************************");

			Annotation ann = iter.next();

			sb.append(analysisFeatureStruct(ann, 0));

		}
		
		return sb.toString();

	}

	static public String analysisFeatureStruct(FeatureStructure fs, int indent) {

		StringBuilder sb = new StringBuilder();
		
		String tabs = "";
		for (int i = 0; i < indent; ++i) {
			tabs += "\t";
		}

		if(fs == null){
			sb.append("\n"+tabs + "type = null" );
			return sb.toString();
		}
		sb.append("\n"+tabs + "type = " + fs.getType().getShortName());
//		System.out.println(tabs + "type = " + fs.getType().getShortName()); // da cancellare
		
		
		if (fs instanceof AnnotationFS) {
			String coveredText = ((AnnotationFS) fs).getCoveredText();
			if (coveredText.length() < 64){
				sb.append("\n"+tabs + "coveredText = " + coveredText); 
//				System.out.println(tabs + "coveredText = " + coveredText); // da cancellare
			}
			else {
				sb.append("\n"+tabs + "coveredText = " + coveredText.substring(0, 64) + "...");
//				System.out.println(tabs + "coveredText = " + coveredText.substring(0, 64) + "..."); // da cancellare
			}
		}

		List<Feature> featList = fs.getType().getFeatures();
		for (Feature feature : featList) {
			
			String rangeType = feature.getRange().getName();
			// sb.append("\n"+tabs+"rangeType == "+rangeType);
			if (CAS.TYPE_NAME_STRING.equals(rangeType)) {
				String value = fs.getStringValue(feature);
				sb.append("\n"+tabs + feature.getShortName() + " -> " + value);
//				System.out.println(tabs + feature.getShortName() + " -> " + value); // da cancellare
			} else if (CAS.TYPE_NAME_INTEGER.equals(rangeType)) {
				int value = fs.getIntValue(feature);
				sb.append("\n"+tabs + feature.getShortName() + " -> " + value);
//				System.out.println(tabs + feature.getShortName() + " -> " + value); // da cancellare
			} else if (CAS.TYPE_NAME_BOOLEAN.equals(rangeType)) {
				boolean value = fs.getBooleanValue(feature);
				sb.append("\n"+tabs + feature.getShortName() + " -> " + value);
//				System.out.println(tabs + feature.getShortName() + " -> " + value); // da cancellare
			} else if (CAS.TYPE_NAME_STRING_ARRAY.equals(rangeType)) {
				StringArray sa = (StringArray) fs.getFeatureValue(feature);
				sb.append("\n"+tabs + feature.getShortName() + " -> ");
				if (sa == null){
					sb.append("null");
					continue;
				}
				String array[] = sa.toArray();
				String value = "[";
				for (int i = 0; i < array.length; ++i) {
					value += array[i];
					if (i != array.length - 1)
						value += ", ";
				}
				value += "]";
				sb.append(value);
//				System.out.println(tabs + feature.getShortName() + " -> " + value); // da cancellare
			} else if (CAS.TYPE_NAME_INTEGER_ARRAY.equals(rangeType)) {
				IntegerArray ia = (IntegerArray) fs.getFeatureValue(feature);
				sb.append("\n"+tabs + feature.getShortName() + " -> ");
				if (ia == null){
					sb.append("null");
					continue;
				}
				int array[] = ia.toArray();
				String value = "[";
				for (int i = 0; i < array.length; ++i) {
					value += array[i];
					if (i != array.length - 1)
						value += ", ";
				}
				value += "]";
				sb.append(value);
//				System.out.println(tabs + feature.getShortName() + " -> " + value); // da cancellare
			} else if (feature.getRange().isArray()) {
				FSArray fsa = (FSArray) fs.getFeatureValue(feature);
				sb.append("\n"+tabs + feature.getShortName() + " ---> ");
				if (fsa == null){
					sb.append("null"); 
					continue;
				}
				FeatureStructure[] array = fsa.toArray();
				for (int i = 0; i < array.length; ++i) {
					sb.append(analysisFeatureStruct(array[i], indent + 1));
				}

			} else {
				sb.append("\n"+tabs + feature.getShortName() + " --> ");
//				System.out.println(tabs + feature.getShortName() + " --> "); // da cancellare
				FeatureStructure value = fs.getFeatureValue(feature);
				if (value != null){
					sb.append(analysisFeatureStruct(value, indent + 1));
				}
			}
		}
		
		
		return sb.toString();

	}
}
