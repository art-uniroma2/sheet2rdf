package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.core.Sheet2RDFCore;
import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.io.File;

public class PearlGeneratorTest extends AbstractTest {

	public static void main(String[] args) throws Exception {
		
		String xlsFilePath = "S2RDFfiles/AgrovocSample.xlsx";
		File excelFile = new File(PearlGeneratorTest.class.getClassLoader().getResource(xlsFilePath).getPath());
		File pearlFile = File.createTempFile("pearl", ".pr");

		//Ontology model
		RepositoryConnection connection = getInitializedConnection();
		
		Sheet2RDFCore s2rdfCore = new Sheet2RDFCore(excelFile, connection);
		String firstSheetName = s2rdfCore.getSheetNames().get(0);
		s2rdfCore.generatePearlFile(pearlFile, firstSheetName);
		System.out.println(S2RDFUtils.pearlFileToString(pearlFile));
		
	}

}
