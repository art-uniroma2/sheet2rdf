package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.core.MappingStruct;
import it.uniroma2.art.sheet2rdf.core.SheetAnnotator;
import it.uniroma2.art.sheet2rdf.core.TypeSystemDescriptionCreator;
import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.rio.RDFParseException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SheetAnnotatorTest extends AbstractTest {

	public static void main(String[] args) throws UIMAException, RDFParseException, RepositoryException, IOException, GenericSheet2RDFException, NotExistingSheetException {
		
		String annotationTypeName = "it.uniroma2.art.Sheet2RDFAnnotation";
		String cellFSName = "it.uniroma2.art.CellFeatureStructure"; 

		String xlsFilePath = "S2RDFfiles/AgrovocSample.xlsx";
		File excelFile = new File(SheetAnnotatorTest.class.getClassLoader().getResource(xlsFilePath).getPath());
		
		RepositoryConnection connection = getInitializedConnection();
		
		Map<String, String> prefixMapping = getRepositoryPrefixMapping(connection);

		S2RDFSheet sheet = SpreadsheetManagerFactory.getSpreadsheetManager(excelFile).getSheet(0);
		List<String> headers = sheet.getHeaders();
		MappingStruct mappingStruct = new MappingStruct(headers, connection, prefixMapping);
		
		TypeSystemDescription tsd = TypeSystemDescriptionCreator.createTypeSystemDescription(mappingStruct, annotationTypeName, cellFSName);
		
		AnalysisEngine ae = AnalysisEngineFactory.createEngine(SheetAnnotator.class,
				SheetAnnotator.EXCEL_FILE, excelFile,
				SheetAnnotator.ANNOTATION_TYPE_STRING, annotationTypeName,
				SheetAnnotator.CELL_FS_NAME_STRING, cellFSName,
				SheetAnnotator.HEADER_FS_MAP, mappingStruct.convertHeaderFsMapToList());
		
		int nRow = sheet.getRowCount();
		String documentText = "";
		for (int i = 0; i < nRow; i++)
			documentText = documentText + ".";
		
		JCas jcas = JCasFactory.createJCas(tsd);
		jcas.setDocumentText(documentText);
		SimplePipeline.runPipeline(jcas, ae);
		
		String report = UIMAUtils.analysisCAS(jcas);
		System.out.println(report);

	}

}
