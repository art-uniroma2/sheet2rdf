package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.core.MappingStruct;
import it.uniroma2.art.sheet2rdf.core.TypeSystemDescriptionCreator;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;
import org.apache.uima.resource.metadata.TypeDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.io.File;
import java.util.List;
import java.util.Map;

public class TypeSystemDescriptionCreatorTest extends AbstractTest {
	
	public static void main(String[] args) throws Exception{
		
		RepositoryConnection connection = getInitializedConnection();
		
		String annotationTypeName = "it.uniroma2.art.ExcelAnnotation";
		String cellFSName = "it.uniroma2.art.CellFeatureStructure";

		String xlsFilePath = "S2RDFfiles/AgrovocSample.xlsx";
		File excelFile = new File(TypeSystemDescriptionCreatorTest.class.getClassLoader().getResource(xlsFilePath).getPath());
		
		Map<String, String> prefixMapping = getRepositoryPrefixMapping(connection);

		S2RDFSheet sheet = SpreadsheetManagerFactory.getSpreadsheetManager(excelFile).getSheet(0);
		List<String> headers = sheet.getHeaders();
		MappingStruct mappingStruct = new MappingStruct(headers, connection, prefixMapping);
		
		TypeSystemDescription tsd = TypeSystemDescriptionCreator.createTypeSystemDescription(mappingStruct, annotationTypeName, cellFSName);
		
		System.out.println("######### TYPES #########");
		TypeDescription[] types = tsd.getTypes();
		for (int i = 0; i < types.length; i++){
			System.out.println(types[i]);
		}
		
	}

}
