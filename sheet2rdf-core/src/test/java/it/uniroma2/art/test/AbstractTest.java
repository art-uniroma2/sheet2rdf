package it.uniroma2.art.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.sheet2rdf.coda.CODAConverter;
import it.uniroma2.art.sheet2rdf.core.MappingStruct;
import it.uniroma2.art.sheet2rdf.core.Sheet2RDFCore;
import it.uniroma2.art.sheet2rdf.header.AdvancedGraphApplication;
import it.uniroma2.art.sheet2rdf.header.GraphApplication;
import it.uniroma2.art.sheet2rdf.header.NodeConversion;
import it.uniroma2.art.sheet2rdf.header.SimpleGraphApplication;
import it.uniroma2.art.sheet2rdf.header.SimpleHeader;

public abstract class AbstractTest {
	
	protected static RepositoryConnection getInitializedConnection() throws RDFParseException, RepositoryException, IOException {
		Repository rep = new SailRepository(new MemoryStore());
		rep.init();
		RepositoryConnection connection = rep.getConnection();
		connection.setNamespace("", "http://art.uniroma2.it/");
		
		SimpleValueFactory vf = SimpleValueFactory.getInstance();
		IRI rdfBaseURI = vf.createIRI("http://www.w3.org/1999/02/22-rdf-syntax-ns");
		IRI rdfsBaseURI = vf.createIRI("http://www.w3.org/2000/01/rdf-schema");
		IRI owlBaseURI = vf.createIRI("http://www.w3.org/2002/07/owl");
		IRI skosBaseURI = vf.createIRI("http://www.w3.org/2004/02/skos/core");
		IRI skosxlBaseURI = vf.createIRI("http://www.w3.org/2008/05/skos-xl");
		connection.add(Sheet2RDFCore.class.getResource("rdf.rdf"), rdfBaseURI.stringValue(), RDFFormat.RDFXML, rdfBaseURI);
		connection.add(Sheet2RDFCore.class.getResource("rdf-schema.rdf"), rdfsBaseURI.stringValue(), RDFFormat.RDFXML, rdfsBaseURI);
		connection.add(Sheet2RDFCore.class.getResource("owl.rdf"), owlBaseURI.stringValue(), RDFFormat.RDFXML, owlBaseURI);
		connection.add(Sheet2RDFCore.class.getResource("skos.rdf"), skosBaseURI.stringValue(), RDFFormat.RDFXML, skosBaseURI);
		connection.add(Sheet2RDFCore.class.getResource("skos-xl.rdf"), skosxlBaseURI.stringValue(), RDFFormat.RDFXML, skosxlBaseURI);
		connection.setNamespace(SKOSXL.PREFIX, SKOSXL.NAMESPACE);
		connection.setNamespace(XMLSchema.PREFIX, XSD.NAMESPACE);
		
		return connection;
	}
	
	protected static Map<String, String> getRepositoryPrefixMapping(RepositoryConnection connection) {
		Map<String, String> prefixMapping = new HashMap<String, String>();
		RepositoryResult<Namespace> namespaces = connection.getNamespaces();
		while (namespaces.hasNext()) {
			Namespace ns = namespaces.next();
			prefixMapping.put(ns.getPrefix(), ns.getName());
		}
		return prefixMapping;
	}
	
	protected static Map<String, String> initializePrefixMapping() {
		Map<String, String> prefixMapping = new HashMap<String, String>();
		prefixMapping.put(RDF.PREFIX, RDF.NAMESPACE);
		prefixMapping.put(RDFS.PREFIX, RDFS.NAMESPACE);
		prefixMapping.put(OWL.PREFIX, OWL.NAMESPACE);
		prefixMapping.put(SKOS.PREFIX, SKOS.NAMESPACE);
		prefixMapping.put(SKOSXL.PREFIX, SKOSXL.NAMESPACE);
		prefixMapping.put("coda", ContractConstants.CODA_CONTRACTS_BASE_URI);
		return prefixMapping;
	}
	
	protected static void printMappingStruct(MappingStruct mappingStruct) {
		List<SimpleHeader> headers = mappingStruct.getHeaders();
		for (SimpleHeader h: headers){
			System.out.println("Header Id:\t" +h.getId());
			System.out.println("header name:\t" + h.getHeaderNameStruct());
			System.out.println("Multiple:\t" + mappingStruct.isHeaderMultiple(h));
			for (NodeConversion c: h.getNodeConversions()) {
				System.out.println("\tconversion) node Id:\t" +c.getNodeId());
				CODAConverter conv = c.getConverter();
				if (conv != null) {
					System.out.println("\t\tconverter) type:\t" +conv.getType());
					System.out.println("\t\tconverter) uri:\t\t" +conv.getContractUri());
					System.out.println("\t\tconverter) lang:\t" +conv.getLanguage());
					System.out.println("\t\tconverter) dt:\t\t" +conv.getDatatypeUri());
					System.out.println("\t\tconverter) par:\t\t" +conv.getParams());
				} else {
					System.out.println("\t\tconverter):\t null");
				}
			}
			for (GraphApplication ga: h.getGraphApplications()) {
				if (ga instanceof SimpleGraphApplication) {
					System.out.println("\tconversion) property:\t" +((SimpleGraphApplication)ga).getProperty());
				} else if (ga instanceof AdvancedGraphApplication) {
					System.out.println("\tconversion) pattern:\t" +((AdvancedGraphApplication)ga).getPattern());
				}
			}
			System.out.println();
		}
	}

}
