package it.uniroma2.art.test;

import it.uniroma2.art.sheet2rdf.core.MappingStruct;
import it.uniroma2.art.sheet2rdf.core.Sheet2RDFCore;
import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HeadersTest extends AbstractTest {

	public static void main(String[] args) throws Exception {
		
		RepositoryConnection connection = getInitializedConnection();
		
		String xlsFilePath = "S2RDFfiles/AgrovocSample.xlsx";
		File excelFile = new File(HeadersTest.class.getClassLoader().getResource(xlsFilePath).getPath());
		System.out.println(excelFile.getAbsolutePath());
		Map<String, MappingStruct> mappingStructs = getMappingStructsFromFile(excelFile, connection);
		
		printMappingStructs(mappingStructs);
	}
	
	private static Map<String, MappingStruct> getMappingStructsFromFile(File excelFile, RepositoryConnection connection) throws GenericSheet2RDFException {
		Sheet2RDFCore s2rdfCore = new Sheet2RDFCore(excelFile, connection);
		return s2rdfCore.getMappingStructMap();
	}

	private static void printMappingStructs(Map<String, MappingStruct> mappingStructMap) {
		mappingStructMap.forEach((key, value) -> {
			System.out.println("Sheet name: " + key);
			printMappingStruct(value);
		});

	}
	
	@SuppressWarnings("unused")
	private static MappingStruct getMappingStructManually(RepositoryConnection connection) {
		List<String> headerList = new ArrayList<>();
		headerList.add("skos:narrower");
		headerList.add("skos:prefLabel@en");
		headerList.add("skos:altLabel@en");
		headerList.add("skos:prefLabel@it");
		headerList.add("skos:prefLabel^^xsd:string");
		headerList.add("preferred label @en");
		headerList.add("preferred label ^^xsd:string");
		
		Map<String, String> prefixMapping = new HashMap<>();
		//prefix mappings of project
		RepositoryResult<Namespace> namespaces = connection.getNamespaces();
		while (namespaces.hasNext()) {
			Namespace ns = namespaces.next();
			prefixMapping.put(ns.getPrefix(), ns.getName());
		}
		
		return new MappingStruct(headerList, connection, prefixMapping);
	}

}
