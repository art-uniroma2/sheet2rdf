package it.uniroma2.art.sheet2rdf.header;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.uniroma2.art.sheet2rdf.coda.CODAConverter;

public class NodeConversion {
	
	private String nodeId; //id of the node produced by the conversion
	private CODAConverter converter;

	private String defaultNamespace; //namespace of the annotation @DefaultNamespace

	private NodeMemoization memoization;
	private NodeSanitization sanitization;

	public NodeConversion() {
		memoization = new NodeMemoization();
		sanitization = new NodeSanitization();
	}

	public String getNodeId() {
		return nodeId;
	}
	
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	public CODAConverter getConverter() {
		return converter;
	}
	
	public void setConverter(CODAConverter converter) {
		this.converter = converter;
	}

	public String getDefaultNamespace() {
		return defaultNamespace;
	}

	public void setDefaultNamespace(String defaultNamespace) {
		this.defaultNamespace = defaultNamespace;
	}

	public NodeMemoization getMemoization() {
		return memoization;
	}

	public void setMemoization(NodeMemoization memoization) {
		this.memoization = memoization;
	}

	public NodeSanitization getSanitization() {
		return sanitization;
	}

	public void setSanitization(NodeSanitization sanitization) {
		this.sanitization = sanitization;
	}

	/**
	 * Check if the node conversion contains a reference to another node id in its converter.
	 * This is useful in order to avoid the serialization in the pearl of a node definition that contains a reference
	 * to another node still not defined
	 * @return
	 */
	public boolean containsNodeReference() {
		return converter.containsNodeReference();
	}
	
	public JsonNode toJson() {
		return new ObjectMapper().valueToTree(this);
	}
	
}
