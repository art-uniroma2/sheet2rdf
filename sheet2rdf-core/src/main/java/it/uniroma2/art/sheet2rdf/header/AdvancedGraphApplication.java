package it.uniroma2.art.sheet2rdf.header;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.uniroma2.art.sheet2rdf.utils.JsonConstants;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class AdvancedGraphApplication extends GraphApplication {
	
	private List<String> nodeIds; //ids of the nodes referenced in the graph pattern
	private Map<String, String> prefixMapping;
	private String pattern;
	private IRI defaultPredicate; //fallback predicate for replacing {{pred}} placeholder
	
	public static final String PREDICATE_PLACEHOLDER = "{{pred}}";
	
	public AdvancedGraphApplication() {
		super();
		nodeIds = new ArrayList<>();
		prefixMapping = new HashMap<>();
	}
	
	public List<String> getNodeIds() {
		return nodeIds;
	}

	public void setNodeIds(List<String> nodeIds) {
		this.nodeIds = nodeIds;
	}
	
	@Override
	public void removeNode(String nodeId) {
		nodeIds.remove(nodeId);
	}
	
	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public Map<String, String> getPrefixMapping() {
		return prefixMapping;
	}

	public void setPrefixMapping(Map<String, String> prefixMapping) {
		this.prefixMapping = prefixMapping;
	}
	
	public IRI getDefaultPredicate() {
		return defaultPredicate;
	}

	public void setDefaultPredicate(IRI defaultPredicate) {
		this.defaultPredicate = defaultPredicate;
	}

	@Override
	public void fromJson(JsonNode jsonNode) {
		ObjectMapper mapper = new ObjectMapper();
		this.id = jsonNode.get(JsonConstants.GRAPH_APPLICATION_ID).asText();
		this.pattern = jsonNode.get(JsonConstants.ADVANCED_GRAPH_APPLICATION_PATTERN).asText();
		this.nodeIds = mapper.convertValue(jsonNode.get(JsonConstants.ADVANCED_GRAPH_APPLICATION_NODE_IDS), new TypeReference<List<String>>(){});
		this.prefixMapping = mapper.convertValue(jsonNode.get(JsonConstants.ADVANCED_GRAPH_APPLICATION_PREFIX_MAPPING), new TypeReference<Map<String, String>>(){});
		JsonNode defaultPredJson = jsonNode.get(JsonConstants.ADVANCED_GRAPH_APPLICATION_DEFAULT_PREDICATE);
		if (defaultPredJson != null && !defaultPredJson.isNull()) {
			this.defaultPredicate = NTriplesUtil.parseURI(defaultPredJson.asText(), SimpleValueFactory.getInstance());
		}
		JsonNode deleteNode = jsonNode.get(JsonConstants.GRAPH_APPLICATION_DELETE);
		if (deleteNode != null && !deleteNode.isNull()) {
			this.delete = deleteNode.asBoolean();
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		s += "id:\t\t" + this.id + "\n";
		s += "pattern:\t" + this.pattern + "\n";
		s += "nodeIds:\t" + this.nodeIds + "\n";
		s += "prefNs:\t\t" + this.prefixMapping + "\n";
		s += "def Pred:\t" + (this.defaultPredicate == null ? "null" : this.defaultPredicate.stringValue());
		return s;
	}
	
}
