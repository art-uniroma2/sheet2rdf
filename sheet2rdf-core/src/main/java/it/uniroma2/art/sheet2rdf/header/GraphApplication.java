package it.uniroma2.art.sheet2rdf.header;

import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import it.uniroma2.art.sheet2rdf.utils.RDF4JValueSerializer;

public abstract class GraphApplication {
	
	protected String id; //id of the object, useful to identify the instance when the user updates it

	protected boolean delete;
	
	
	public GraphApplication() {
		id = UUID.randomUUID().toString();
	}
	
	public String getId() {
		return id;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	/**
	 * Removes the referenced node with the given id
	 * @param nodeId
	 */
	abstract public void removeNode(String nodeId);
	
	public JsonNode toJson() {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(IRI.class, new RDF4JValueSerializer());
		mapper.registerModule(module);
		return mapper.valueToTree(this);
	}
	
	abstract public void fromJson(JsonNode jsonNode);
	
	abstract public String toString();

}
