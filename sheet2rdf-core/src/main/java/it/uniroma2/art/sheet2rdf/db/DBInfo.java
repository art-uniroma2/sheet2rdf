package it.uniroma2.art.sheet2rdf.db;

import java.util.List;

public class DBInfo {

    private final String db_base_url;
    private final String db_name;
    private final List<String> db_table;
    private final String db_user;
    private final String db_password;
    private final AvailableDBDriverName.Values db_driverName;

    public DBInfo(String db_base_url, String db_name, List<String> db_table, String db_user, String db_password, AvailableDBDriverName.Values db_driverName) {
        this.db_base_url = db_base_url;
        this.db_name = db_name;
        this.db_table = db_table;
        this.db_user = db_user;
        this.db_password = db_password;
        this.db_driverName = db_driverName;
    }

    public String getDb_base_url() {
        return db_base_url;
    }

    public String getDb_name() {
        return db_name;
    }

    public List<String> getDb_tableList() {
        return db_table;
    }

    public String getDb_user() {
        return db_user;
    }

    public String getDb_password() {
        return db_password;
    }

    public AvailableDBDriverName.Values getDb_driverName() {
        return db_driverName;
    }

    public String getDb_driverNameLC() {
        return db_driverName.name().toLowerCase();
    }

    public String getDelimiterFromDriverName() {
        if (db_driverName.equals(AvailableDBDriverName.Values.PostgreSQL)) {
            return "\"";
        } else { // AvailableDBDriverName.Values.MySQL or AvailableDBDriverName.Values.MariaDB
            return "`";
        }
    }
}
