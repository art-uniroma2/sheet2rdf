package it.uniroma2.art.sheet2rdf.sheet.csv;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.sheet.SheetManager;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CsvSheetManager implements SheetManager {

    private static final char[] popularAlternativeDelimiters = {'|', ';'};

    private List<CSVRecord> records;

    public CsvSheetManager(File file) {
        try {
            Reader in = new FileReader(file);
            CSVParser parser = null;
            if (testFormat(file, CSVFormat.MYSQL)){
                parser = CSVFormat.MYSQL.parse(in);
            } else if (testFormat(file, CSVFormat.RFC4180)){
                parser = CSVFormat.RFC4180.parse(in);
            } else if (testFormat(file, CSVFormat.TDF)){
                parser = CSVFormat.TDF.parse(in);
            } else if (testFormat(file, CSVFormat.EXCEL)){
                parser = CSVFormat.EXCEL.parse(in);
            } else if (testFormat(file, CSVFormat.DEFAULT)){
                parser = CSVFormat.DEFAULT.parse(in);
            } else {
                for (char delim : popularAlternativeDelimiters){
                    CSVFormat customFormat = CSVFormat.newFormat(delim).withEscape('\\').withQuote('"').withRecordSeparator('\n');
                    if (testFormat(file, customFormat)){
                        parser = customFormat.parse(in);
                        break;
                    }
                }
                if (parser == null) {
                    parser = CSVFormat.DEFAULT.parse(in);
                }
            }
            records = parser.getRecords();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getSheetName() {
        return "CSV"; //csv doesn't support multiple sheets, so the only sheet is hardcoded named CSV
    }

    @Override
    public List<String> getHeaders(boolean includeDuplicate) {
        List<String> headers = new ArrayList<String>();
        Iterator<CSVRecord> it = records.iterator();
        if (it.hasNext()){
            CSVRecord record = it.next();
            for (String header : record){
                if (!headers.contains(header))//if the headers is not yet in the list, add it
                    headers.add(header);
                else { //else add it only if includeDuplicate = true
                    if (includeDuplicate)
                        headers.add(header);
                }
            }
        }
        return headers;
    }

    @Override
    public List<List<String>> getDataTable() throws GenericSheet2RDFException {
        int rows = getRowCount();
        int columns = getColumnCount();
        List<List<String>> table = new ArrayList<>();
        for (int r = 1; r < rows; r++){
            List<String> arrayListRow = new ArrayList<>();
            for (int c = 0; c < columns; c++){
                String data = getCellValue(r, c);
                arrayListRow.add(data);
            }
            table.add(arrayListRow);
        }
        return table;
    }

    @Override
    public String getCellValue(int row, int column) {
        return records.get(row).get(column);
    }

    @Override
    public boolean isMultipleHeader(String headerValue) {
        List<String> headers = getHeaders(true);
        //find the column of headerValue
        for (int i = 0; i < headers.size(); i++){
            String h = headers.get(i);
            if (h.equals(headerValue)){//once found, check if the following column has the same header
                if (i+1 != headers.size()){
                    if (headers.get(i+1).equals(headerValue))
                        return true;
                }
                break;
            }
        }
        return false;
    }

    @Override
    public int getColumnCount() {
        return records.get(0).size();
    }

    @Override
    public int getRowCount() {
        return records.size();
    }

    /**
     * Make sure the reader has correct delimiter and quotation set.
     * Check first line and make sure that all the other rows have the same amount of columns and at least 2
     *
     * @param file
     * @param format
     * @return
     * @throws IOException
     */
    private static boolean testFormat(File file, CSVFormat format) throws IOException {
        boolean valid = true;
        Reader reader = new FileReader(file);

        int MIN_COLUMNS = 2;
        int MAX_ROWS = 50;

        CSVParser parser = format.parse(reader);
        List<CSVRecord> records = parser.getRecords();

        if (records.size() == 0)
            valid = false;

        //get number of headers columns (min 2)
        int headCols = records.get(0).size();
        if (headCols < MIN_COLUMNS)
            valid = false;

        int checkRows = MAX_ROWS;
        if (records.size() < checkRows)
            checkRows = records.size();
        //check if every row has the same columns of header (limit to first 50 rows if there are more than that)
        for (int i=1; i<checkRows; i++){
            if (records.get(i).size() != headCols)
                valid = false;
        }

        return valid;
    }

}
