package it.uniroma2.art.sheet2rdf.header;

public class RemovePunctuation {

    Boolean enabled; //Boolean instead of boolean since it can be left unspecified (fallback to the default behavior)
    String chars;

    public RemovePunctuation() {
    }

    public RemovePunctuation(boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getChars() {
        return chars;
    }

    public void setChars(String chars) {
        this.chars = chars;
    }
}
