package it.uniroma2.art.sheet2rdf.resolver;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.query.Binding;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.queryrender.RenderUtils;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.util.ArrayList;
import java.util.Collection;

public class PropertyRangeResolver {
	
	private final RepositoryConnection repoConnection;
	
	public PropertyRangeResolver(RepositoryConnection repoConnection){
		this.repoConnection = repoConnection;
	}

	/**
	 * Given a property looks for its range into the model passed to the constructor. 
	 * If no range is found, it returns null.
	 *
	 * Note: this is used during the initialization of an header structure just to estimate which converter will be
	 * used (between uri or literal) in the nodeConversion. So this method doesn't need to be 100% precise, if multiple
	 * range are found for a property, then get just one. Or even if the property could inherit other ranges from
	 * its parent properties, just ignore them.
	 * @param property
	 * @return
	 */
	public IRI getRange(IRI property) {
		Multimap<IRI, IRI> propSuperMap = HashMultimap.create(); //property->superProp[]
		Multimap<IRI, IRI> propRangeMap = HashMultimap.create(); //property->range[]
		Collection<IRI> checkedProps = new ArrayList<>(); //list of properties already considered (prevent loop)

		// @formatter:off
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
				"SELECT ?property ?superProperty ?range WHERE {				\n" +
				"	{ 														\n" +
				"		BIND(%property% as ?prop)							\n" +
				"		?prop rdfs:subPropertyOf* ?property	.				\n" +
				"		?property rdfs:subPropertyOf* ?superProperty .		\n" +
				"		OPTIONAL {											\n" +
				"			?property rdfs:range ?range .					\n" +
				"			FILTER isIRI(?range)							\n" +
				"		}													\n" +
				"	}														\n" +
				"	UNION													\n" +
				"	{														\n" +
				"		BIND(%property% as ?property)						\n" +
				"		?property rdfs:range ?range .						\n" +
				"		FILTER isIRI(?range)								\n" +
				"	}														\n" +
				"}";
		// @formatter:on
		query = query.replace("%property%", RenderUtils.toSPARQL(property));
		TupleQuery tq = repoConnection.prepareTupleQuery(query);
		TupleQueryResult results = tq.evaluate();
		while (results.hasNext()) {
			BindingSet bs = results.next();
			IRI prop = (IRI) bs.getValue("property");
			Binding superPropBinding = bs.getBinding("superProperty");
			if (superPropBinding != null) {
				IRI superProp = (IRI) superPropBinding.getValue();
				if (!superProp.equals(prop)) {
					propSuperMap.put(prop, superProp);
				}
			}
			Binding rangeBinding = bs.getBinding("range");
			if (rangeBinding != null) {
				IRI r = (IRI) rangeBinding.getValue();
				propRangeMap.put(prop, r);
			}
		}
		if (propRangeMap.isEmpty()) { //if no property has range, return immediately null
			return null;
		}
		return resolveRange(property, propSuperMap, propRangeMap, checkedProps);
	}

	private IRI resolveRange(IRI property, Multimap<IRI, IRI> propSuperMap, Multimap<IRI, IRI> propRangeMap, Collection<IRI> checkedProps) {
		//prevent infinite loop
		if (checkedProps.contains(property)) {
			return null;
		} else {
			checkedProps.add(property);
		}

		//by first, try to get directly the range of the input property
		Collection<IRI> ranges = propRangeMap.get(property);
		if (ranges != null && ranges.size() > 0) { //property has range
			return ranges.iterator().next(); //I don't care to get multiple ranges, returns the first
		} else { //property doesn't have range, try to resolve the range of its superProps
			Collection<IRI> superProps = propSuperMap.get(property);
			if (superProps != null && superProps.size() > 0) {
				for (IRI superProp : superProps) {
					IRI range = resolveRange(superProp, propSuperMap, propRangeMap, checkedProps);
					if (range != null) {
						return range;
					}
				}
			}
			return null; //if this code is reached, property has no superProp or all superProp have no range
		}
	}

}
