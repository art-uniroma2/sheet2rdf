package it.uniroma2.art.sheet2rdf.core;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.sheet2rdf.db.AvailableDBDriverName;
import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SheetManager;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;
import it.uniroma2.art.sheet2rdf.sheet.db.DBTableManager;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.CasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;

import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;

/**
 * Annotator UIMA. This class creates annotations structured with the TypeSystemDescription created with
 * TypeSystemDescriptionCreator
 *
 * @author Tiziano Lorenzetti
 */
public class DBTableAnnotator extends CasAnnotator_ImplBase {

    private static final String VALUE_FEATURE_NAME = "value";
    private static final String ROW_FEATURE_NAME = "row";
    private static final String COLUMN_FEATURE_NAME = "column";

    public static final String DB_BASE_URL = "dbBaseUrl";
    @ConfigurationParameter(name = DB_BASE_URL)
    private String dbBaseUrl;

    public static final String DB_NAME = "dbName";
    @ConfigurationParameter(name = DB_NAME)
    private String dbName;

    public static final String DB_TABLE_LIST = "dbTableList";
    @ConfigurationParameter(name = DB_TABLE_LIST)
    private List<String> dbTableList;

    public static final String DB_TABLE = "dbTable";
    @ConfigurationParameter(name = DB_TABLE)
    private String dbTable;

    public static final String DB_USER = "dbUser";
    @ConfigurationParameter(name = DB_USER)
    private String dbUser;

    public static final String DB_PASS = "dbPass";
    @ConfigurationParameter(name = DB_PASS)
    private String dbPass;

    public static final String DB_DRIVER_NAME = "dbDriverName";
    @ConfigurationParameter(name = DB_DRIVER_NAME)
    private String dbDriverName;

    public static final String ANNOTATION_TYPE_STRING = "annotationType";
    @ConfigurationParameter(name = ANNOTATION_TYPE_STRING)
    private String annotationType;

    public static final String CELL_FS_NAME_STRING = "cellFSName";
    @ConfigurationParameter(name = CELL_FS_NAME_STRING)
    private String cellFSName;

    /*
     * UIMA Fit doesn't provide an easy way to pass a Map as parameter to an annotator.
     * As workaround, I pass the Map <normalized_headerName, fs_id> as a List (accepted as parameter) where the
     * even elements are the keys and the odd elements are the values. The List will be converted to map during
     * the execution of the annotator.
     */
    public static final String HEADER_FS_MAP = "headerFsMap_asList";
    @ConfigurationParameter(name = HEADER_FS_MAP)
    private List<String> headerFsMap_asList;

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {

        TypeSystem ts = aCAS.getTypeSystem();
        Type type = ts.getType(annotationType);
        Type cellFSType = ts.getType(cellFSName);

        SpreadsheetOrDb spreadsheetOrDb = new SpreadsheetOrDb(dbBaseUrl, dbName, dbTableList, dbUser, dbPass,
                AvailableDBDriverName.Values.valueOf(dbDriverName));
        S2RDFSheet sheet;
        try {
            sheet = SpreadsheetManagerFactory.getSpreadsheetManager(spreadsheetOrDb).getSheet(dbTable);
        } catch (NotExistingSheetException | GenericSheet2RDFException e) {
            throw new AnalysisEngineProcessException(e);
        }

//        DBTableManager dbTableManager = new DBTableManager(spreadsheetOrDb, dbTable);

        //convert header-id mapping list into map
        Map<String, String> headerFsMap = new HashMap<>();
        for (int i = 0; i < headerFsMap_asList.size(); i += 2) {
            headerFsMap.put(headerFsMap_asList.get(i), headerFsMap_asList.get(i + 1));
        }

        List<String> headersList = sheet.getHeaders();
//        List<List<String>> tableContent = sheet.getTableContent();
        int nColumn = sheet.getColumnCount();
        int nRow = sheet.getRowCount();

        //for every row create an annotation and populate with the data fetched from the cells
        for (int i = 0; i < nRow; ++i) {
            //create annotation ExcelAnnotation
            AnnotationFS ann = aCAS.createAnnotation(type, i, i);//i as begin and end (last 2 params)

            FeatureStructure valueFS;
            Feature valueFeat = cellFSType.getFeatureByBaseName(VALUE_FEATURE_NAME);
            Feature rowFeat = cellFSType.getFeatureByBaseName(ROW_FEATURE_NAME);
            Feature columnFeat = cellFSType.getFeatureByBaseName(COLUMN_FEATURE_NAME);

            //populating features based on headers
            for (int j = 0; j < nColumn; j++) {
                String headerName = headersList.get(j);
                Feature feat = type.getFeatureByBaseName(headerFsMap.get(S2RDFUtils.getHeaderId(headerName, j)));
                String value = sheet.getCellValue(i, j);
                if (!value.equals("")) {
                    valueFS = aCAS.createFS(cellFSType);
                    valueFS.setStringValue(valueFeat, value);
                    valueFS.setIntValue(rowFeat, i);
                    valueFS.setIntValue(columnFeat, j);
                    ann.setFeatureValue(feat, valueFS);
//					System.out.println("Added "+value+" to feature "+feat+" row "+ valueFS.getIntValue(rowFeat) +" col "+valueFS.getIntValue(columnFeat));
                }
            }
            aCAS.addFsToIndexes(ann);
        }
    }

}
