package it.uniroma2.art.sheet2rdf.exception;

public class InvalidWizardStatusException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6246490065823816823L;
	
	public InvalidWizardStatusException(String msg) {
		super(msg);
	}

}
