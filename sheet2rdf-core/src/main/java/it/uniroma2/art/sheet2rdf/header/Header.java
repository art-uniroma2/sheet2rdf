package it.uniroma2.art.sheet2rdf.header;

public abstract class Header {
	
	protected String id; //header normalized
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
