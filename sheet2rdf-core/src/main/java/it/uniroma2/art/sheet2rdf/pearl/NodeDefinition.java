package it.uniroma2.art.sheet2rdf.pearl;

import it.uniroma2.art.sheet2rdf.coda.CODAConverter;
import it.uniroma2.art.sheet2rdf.header.NodeMemoization;
import it.uniroma2.art.sheet2rdf.header.NodeSanitization;

public class NodeDefinition {

	private String placeholder;
	private String fsValue;
	private CODAConverter converter;

	private String defaultNamespace; //namespace of the annotation @DefaultNamespace

	private NodeMemoization memoization;
	private NodeSanitization sanitization;

	
	public NodeDefinition(String placeholder, CODAConverter converter, String fsValue){
		this.placeholder = placeholder;
		this.converter = converter;
		this.fsValue = fsValue;
		memoization = new NodeMemoization();
		sanitization = new NodeSanitization();
	}
	
	public String getPlaceholder() {
		return placeholder;
	}
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
	
	public CODAConverter getConverter() {
		return converter;
	}
	public void setConverter(CODAConverter converter) {
		this.converter = converter;
	}

	public String getDefaultNamespace() {
		return defaultNamespace;
	}

	public void setDefaultNamespace(String defaultNamespace) {
		this.defaultNamespace = defaultNamespace;
	}
	
	public String getFsValue() {
		return fsValue;
	}
	public void setFsValue(String fsValue) {
		this.fsValue = fsValue;
	}

	public NodeMemoization getMemoization() {
		return memoization;
	}

	public void setMemoization(NodeMemoization memoization) {
		this.memoization = memoization;
	}

	public NodeSanitization getSanitization() {
		return sanitization;
	}

	public void setSanitization(NodeSanitization sanitization) {
		this.sanitization = sanitization;
	}
}
