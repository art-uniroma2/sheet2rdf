package it.uniroma2.art.sheet2rdf.header;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import com.fasterxml.jackson.databind.JsonNode;

import it.uniroma2.art.sheet2rdf.utils.JsonConstants;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class SimpleGraphApplication extends GraphApplication {

    private IRI property; //predicate of the s-p-o triple in the graph application
    private String nodeId; //id of the node that represent the object in the s-p-o triple defined by the graph application
    //usually $subject <pred> <nodeId> for generic header
    private IRI type; //optional type to specify for the nodeId (will add a further triple <nodeId> a <type>)
    private Value value; //object of the s-p-o in the graph application. If provided over the nodeId, will generate a triple
    //$subject <pred> <value>


    public SimpleGraphApplication() {
        super();
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @Override
    public void removeNode(String nodeId) {
        if (this.nodeId.equals(nodeId)) {
            this.nodeId = null;
        }
    }

    public IRI getProperty() {
        return property;
    }

    public void setProperty(IRI property) {
        this.property = property;
    }

    public IRI getType() {
        return type;
    }

    public void setType(IRI type) {
        this.type = type;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public void fromJson(JsonNode jsonNode) {
        SimpleValueFactory svf = SimpleValueFactory.getInstance();
        this.id = jsonNode.get(JsonConstants.GRAPH_APPLICATION_ID).asText();
        JsonNode propertyNode = jsonNode.get(JsonConstants.SIMPLE_GRAPH_APPLICATION_PROPERTY);
        if (propertyNode != null && !propertyNode.isNull()) {
            this.property = NTriplesUtil.parseURI(propertyNode.asText(), svf);
        }
        JsonNode typeNode = jsonNode.get(JsonConstants.SIMPLE_GRAPH_APPLICATION_TYPE);
        if (typeNode != null && !typeNode.isNull()) {
            this.type = NTriplesUtil.parseURI(typeNode.asText(), svf);
        }

        JsonNode valueNode = jsonNode.get(JsonConstants.SIMPLE_GRAPH_APPLICATION_VALUE);
        if (valueNode != null && !valueNode.isNull()) {
            this.value = NTriplesUtil.parseURI(valueNode.asText(), svf);
        }
        this.nodeId = jsonNode.get(JsonConstants.SIMPLE_GRAPH_APPLICATION_NODE_ID).asText();

        JsonNode deleteNode = jsonNode.get(JsonConstants.GRAPH_APPLICATION_DELETE);
        if (deleteNode != null && !deleteNode.isNull()) {
            this.delete = deleteNode.asBoolean();
        }
    }

    @Override
    public String toString() {
        String s = "";
        s += "id:\t" + this.id + "\n";
        s += "prop:\t" + (this.property != null ? this.property.stringValue() : null) + "\n";
        s += "type:\t" + (this.type != null ? this.type.stringValue() : null) + "\n";
        s += "value:\t" + (this.value != null ? this.value.stringValue() : null) + "\n";
        s += "nodeId:\t" + this.nodeId;
        return s;
    }
}
