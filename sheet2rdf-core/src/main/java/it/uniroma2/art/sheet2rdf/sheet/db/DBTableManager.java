package it.uniroma2.art.sheet2rdf.sheet.db;

import it.uniroma2.art.sheet2rdf.core.SpreadsheetOrDb;
import it.uniroma2.art.sheet2rdf.db.AvailableDBDriverName;
import it.uniroma2.art.sheet2rdf.db.DBInfo;
import it.uniroma2.art.sheet2rdf.exception.DBLocalizedException;
import it.uniroma2.art.sheet2rdf.sheet.SheetManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBTableManager implements SheetManager {
    private final SpreadsheetOrDb spreadsheetOrDb;

    private final String tableName;

    public DBTableManager(SpreadsheetOrDb spreadsheetOrDb, String tableName) {
        this.spreadsheetOrDb = spreadsheetOrDb;
        this.tableName = tableName;
        if (!spreadsheetOrDb.isForDb()) {
            // this should not happen, since this SheetManager is used to interact with a table of a DB, so throw an
            // exception
            //TODO find a better way to manage such problem
            System.err.println("ERROR: the input spreadsheetOrDb is not about a DB");
        }
    }

    @Override
    public String getSheetName() {
        return tableName;
    }

    @Override
    public List<String> getHeaders(boolean includeDuplicate) throws DBLocalizedException {
        return getTableHeaders();
    }

    @Override
    public List<List<String>> getDataTable() throws DBLocalizedException {
        return getValuesFromTable();
    }

    @Override
    public String getCellValue(int row, int column) {
        return null;
    }

    @Override
    public boolean isMultipleHeader(String headerValue) {
        return false;
    }

    @Override
    public int getColumnCount() throws DBLocalizedException {
        return getTableHeaders().size();
    }

    @Override
    public int getRowCount() throws DBLocalizedException {
        return getValuesFromTable().size();
    }


    private ArrayList<String> getTableHeaders() throws DBLocalizedException {
        ArrayList<String> colNameList = new ArrayList<>();
        DBInfo dbInfo = spreadsheetOrDb.getDbInfo();

        try {
            Class.forName(AvailableDBDriverName.getClassNameFromDriver(dbInfo.getDb_driverName()));
        } catch (ClassNotFoundException e) {
            //TODO decide what to do, but this should never happen, in theory
            e.printStackTrace();
        }
        String delimiter = dbInfo.getDelimiterFromDriverName();

        String db_url ="jdbc:"+dbInfo.getDb_driverNameLC()+"://"+dbInfo.getDb_base_url()+"/"
                + dbInfo.getDb_name();

        String query = "SELECT * FROM "+delimiter+tableName +delimiter+" LIMIT 1";


        try(Connection conn = DriverManager.getConnection(db_url, dbInfo.getDb_user(),
                dbInfo.getDb_password());
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query)) {
            ResultSetMetaData resultSetMetaData = rs.getMetaData();

            //get the column name
            int columnCount = resultSetMetaData.getColumnCount();

            for (int i=1; i<=columnCount; ++i){
                colNameList.add(resultSetMetaData.getColumnName(i));
            }

        } catch (SQLException e) {
            throw new DBLocalizedException(e);
        }

        return colNameList;
    }

    private List<List<String>> getValuesFromTable() throws DBLocalizedException {
        List<List<String>> valueListOfList = new ArrayList<>();
        DBInfo dbInfo = spreadsheetOrDb.getDbInfo();
        String delimiter = dbInfo.getDelimiterFromDriverName();

        // g the table headers, which are used to have always the same results order
        List<String> colNameList = getTableHeaders();

        //execute a query to get all the data from the table
        String db_url ="jdbc:"+dbInfo.getDb_driverNameLC()+"://"+dbInfo.getDb_base_url()+"/"
                + dbInfo.getDb_name();

        StringBuilder queryBuilder = new StringBuilder();

        queryBuilder.append("SELECT * FROM ").append(delimiter).append(tableName).append(delimiter).append("");

        // create the query ordering the results using the colNameList
        boolean fist = true;
        queryBuilder.append(" ORDER BY");
        for(String colName : colNameList) {
            if (!fist) {
                queryBuilder.append(",");
            } else {
                fist = false;
            }

            queryBuilder.append(" ").append(delimiter).append(colName).append(delimiter).append("");

        }

        String query = queryBuilder.toString();
        try(Connection conn = DriverManager.getConnection(db_url, dbInfo.getDb_user(),
                dbInfo.getDb_password());
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query)) {
            ResultSetMetaData resultSetMetaData = rs.getMetaData();

            //get the column name
            int columnCount = resultSetMetaData.getColumnCount();
            List<String> columnNameList = new ArrayList<>();

            for (int i=1; i<=columnCount; ++i){
                columnNameList.add(resultSetMetaData.getColumnName(i));
            }

            // Extract data from result set

            while (rs.next()) {
                ArrayList<String> valueList = new ArrayList<>();
                for (int i=0; i<columnCount; ++i) {
                    String value = getValueFromCell(rs, i, columnNameList);
                    valueList.add(value);
                }
                valueListOfList.add(valueList);
            }
        } catch (SQLException e) {
            throw new DBLocalizedException(e);
        }
        return valueListOfList;
    }

    private String getValueFromCell(ResultSet rs, int pos, List<String> columnNameList)
            throws SQLException {
        if(rs.getString(columnNameList.get(pos)) == null) {
            // if the value is null, return an empty string
            return "";
        }
        return rs.getString(columnNameList.get(pos));
    }
}
