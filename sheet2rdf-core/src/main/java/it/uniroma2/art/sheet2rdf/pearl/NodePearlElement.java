package it.uniroma2.art.sheet2rdf.pearl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.sheet2rdf.coda.CODAConverter;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class NodePearlElement implements PearlElement {

    private final List<NodeDefinition> nodeDefinition;

    public NodePearlElement() {
        nodeDefinition = new ArrayList<>();
    }

    /**
     * Append a node triple to the NodePearlElement
     *
     * @param placeholder
     * @param converter
     * @param fsValue
     */
    public void add(String placeholder, CODAConverter converter, String fsValue) {
        this.add(placeholder, converter, fsValue, false);
    }

    /**
     * Append a node definition to the NodePearlElement
     *
     * @param node
     */
    public void add(NodeDefinition node) {
        this.add(node, false);
    }

    /**
     * Add a node triple to the NodePearlElement. If addBefore is true, the new triple is added at the beginning and
     * the other elements are unshifted
     *
     * @param placeholder
     * @param converter
     * @param fsValue
     * @param addBefore
     */
    public void add(String placeholder, CODAConverter converter, String fsValue, boolean addBefore) {
        if (addBefore) {
            nodeDefinition.add(0, new NodeDefinition(placeholder, converter, fsValue));
        } else {
            nodeDefinition.add(new NodeDefinition(placeholder, converter, fsValue));
        }
    }

    /**
     * Add a node definition to the NodePearlElement. If addBefore is true, the new node is added at the beginning and
     * the other elements are unshifted
     *
     * @param node
     * @param addBefore
     */
    public void add(NodeDefinition node, boolean addBefore) {
        if (addBefore) {
            nodeDefinition.add(0, node);
        } else {
            nodeDefinition.add(node);
        }
    }

    /**
     * Returns the NodeTriple that defines the given placeholder
     *
     * @param placeholder
     * @return
     */
    public NodeDefinition getNodeDefinition(String placeholder) {
        for (NodeDefinition t : nodeDefinition) {
            if (t.getPlaceholder().equals(placeholder)) {
                return t;
            }
        }
        return null;
    }

    @Override
    public String serialize(String tabs, Map<String, String> prefixMapping) {
        String output = "";
        for (NodeDefinition nodeDef : nodeDefinition) {
            String placeholder = nodeDef.getPlaceholder();

            CODAConverter converter = nodeDef.getConverter();
            String converterSerialization = converter != null ? converter.serialize(prefixMapping) : "%pls_provide_a_converter%";

            if (nodeDef.getDefaultNamespace() != null) {
                output += tabs + "@DefaultNamespace("
                        + NTriplesUtil.toNTriplesString(SimpleValueFactory.getInstance().createIRI(nodeDef.getDefaultNamespace()))
                        + ")\n";
            }

            //sanitization flags have been set only if needed to be written (whatever the value is), so if not null, write them
            if (nodeDef.getSanitization().getTrim() != null) {
                output += tabs + nodeDef.getSanitization().serializeTrim() + "\n";
            }
            if (nodeDef.getSanitization().getRemoveDuplicateSpaces() != null) {
                output += tabs + nodeDef.getSanitization().serializeRemoveDuplicateSpaces() + "\n";
            }
            if (nodeDef.getSanitization().getRemovePunctuation().getEnabled() != null) {
                output += tabs + nodeDef.getSanitization().serializeRemovePunctuation() + "\n";
            }
            if (nodeDef.getSanitization().getUpperCase() != null) {
                output += tabs + nodeDef.getSanitization().serializeUpperCase() + "\n";
            }
            if (nodeDef.getSanitization().getLowerCase() != null) {
                output += tabs + nodeDef.getSanitization().serializeLowerCase() + "\n";
            }
            if (nodeDef.getMemoization().isEnabled()) {
                output += tabs + "@Memoized";
                List<String> memoizedParams = new ArrayList<>();
                if (nodeDef.getMemoization().getId() != null) {
                    memoizedParams.add(nodeDef.getMemoization().getId());
                }
                if (!nodeDef.getMemoization().isIgnoreCase()) {
                    memoizedParams.add("ignorecase=" + nodeDef.getMemoization().isIgnoreCase());
                }
                if (!memoizedParams.isEmpty()) {
                    output += "(" + String.join(",", memoizedParams) + ")";
                }
                output += "\n";
            }

            String fsValue = nodeDef.getFsValue();
            if (fsValue == null) {
                fsValue = "%pls_provide_a_fs%";
            } else {
                fsValue += "/value";
            }

            output += tabs + placeholder + " " + converterSerialization + " " + fsValue + " .\n";
        }
        return output;
    }

}
