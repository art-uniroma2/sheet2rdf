package it.uniroma2.art.sheet2rdf.header;

public class NodeMemoization {

    private boolean enabled;
    private String id;
    private boolean ignoreCase;

    public NodeMemoization() {
        this(false);
    }

    public NodeMemoization(boolean enabled) {
        this.enabled = enabled;
        this.ignoreCase = true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }
}
