package it.uniroma2.art.sheet2rdf.sheet.csv;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.sheet.AbstractSpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class CsvSpreadsheetManager extends AbstractSpreadsheetManager {

    public CsvSpreadsheetManager(File file) throws GenericSheet2RDFException {
        sheetsMap = new LinkedHashMap<>();
        prefixMappings = new HashMap<>(); //csv has no prefix mappings sheet, so the map will stay empty
        S2RDFSheet sheet = new S2RDFSheet(new CsvSheetManager(file));
        sheetsMap.put("CSV", sheet);
    }

}
