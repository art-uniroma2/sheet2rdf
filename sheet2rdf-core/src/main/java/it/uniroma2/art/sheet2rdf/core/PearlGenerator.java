package it.uniroma2.art.sheet2rdf.core;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.sheet2rdf.header.NodeSanitization;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.RDF;

import it.uniroma2.art.coda.structures.ValueOrString;
import it.uniroma2.art.sheet2rdf.header.AdvancedGraphApplication;
import it.uniroma2.art.sheet2rdf.header.GraphApplication;
import it.uniroma2.art.sheet2rdf.header.NodeConversion;
import it.uniroma2.art.sheet2rdf.header.SimpleGraphApplication;
import it.uniroma2.art.sheet2rdf.header.SimpleHeader;
import it.uniroma2.art.sheet2rdf.header.SubjectHeader;
import it.uniroma2.art.sheet2rdf.pearl.GraphPearlElement;
import it.uniroma2.art.sheet2rdf.pearl.GraphPearlElementPatternBased;
import it.uniroma2.art.sheet2rdf.pearl.GraphPearlElementTripleBased;
import it.uniroma2.art.sheet2rdf.pearl.NodeDefinition;
import it.uniroma2.art.sheet2rdf.pearl.NodePearlElement;
import it.uniroma2.art.sheet2rdf.pearl.PearlBuilder;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class PearlGenerator {

    private final String annotationType;

    /**
     * @param annotationType
     */
    public PearlGenerator(String annotationType) {
        this.annotationType = annotationType;
    }

    /**
     * Generates and writes pearl code into the given pearlFile.
     *
     * @param pearlFile
     * @param ms
     * @param prefixMapping
     * @throws IOException
     */
    public void generatePearl(File pearlFile, MappingStruct ms, Map<String, String> prefixMapping) throws IOException {
        List<NodePearlElement> nodeSection = buildNodeSection(ms);
        List<GraphPearlElement> graphSection = buildGraphSection(ms);
        Map<String, String> fullPrefixMapping = buildPrefixMappings(ms, prefixMapping);
        PearlBuilder pearlBuilder = new PearlBuilder(fullPrefixMapping, annotationType, ms.getRuleSanitization(), nodeSection, graphSection);
        String pearlCode = pearlBuilder.serialize();
        PrintWriter pw = new PrintWriter(pearlFile);
        pw.append(pearlCode);
        pw.close();
    }

    /**
     * Merge the s2rdf prefix mappings (repository + sheet) with the mappings defined in the AdvancedGraphApplication (if any)
     *
     * @param ms
     * @param prefixMapping
     * @return
     */
    private Map<String, String> buildPrefixMappings(MappingStruct ms, Map<String, String> prefixMapping) {
        Map<String, String> fullMappings = new HashMap<>(prefixMapping);
        for (SimpleHeader h : ms.getHeaders()) {
            if (h.isIgnore()) continue;
            for (GraphApplication g : h.getGraphApplications()) {
                if (g instanceof AdvancedGraphApplication) {
                    AdvancedGraphApplication aga = (AdvancedGraphApplication) g;
                    for (Entry<String, String> mapping : aga.getPrefixMapping().entrySet()) {
                        if (!fullMappings.containsKey(mapping.getKey())) {
                            fullMappings.put(mapping.getKey(), mapping.getValue());
                        }
                    }
                }
            }
        }
        return fullMappings;
    }

    private List<NodePearlElement> buildNodeSection(MappingStruct ms) {
        List<NodePearlElement> nodeSection = new ArrayList<>();

        //subject
        SubjectHeader subjHead = ms.getSubjectHeader();
        NodeConversion subjectConversion = subjHead.getNodeConversion();
        NodeDefinition nodeDef = new NodeDefinition(subjectConversion.getNodeId(),
                subjectConversion.getConverter(), ms.getFeatureStructName(subjHead));

        setNodeDefaultNamespace(nodeDef, subjectConversion);

        setNodeSanitization(nodeDef, subjectConversion.getSanitization(), ms.getRuleSanitization());
        nodeDef.setMemoization(subjectConversion.getMemoization());

        NodePearlElement npe = new NodePearlElement();
        npe.add(nodeDef);
        nodeSection.add(npe);

        //headers
        for (SimpleHeader h : ms.getHeaders()) {
            if (h.isIgnore()) continue;

            List<NodeConversion> nodeConversions = h.getNodeConversions();
            nodeConversions.sort(new NodeConversionComparator()); //sort the node conversion, postponing those which contain node references
            for (NodeConversion nodeConv : h.getNodeConversions()) {
                nodeDef = new NodeDefinition(nodeConv.getNodeId(), nodeConv.getConverter(), ms.getFeatureStructName(h));

                setNodeDefaultNamespace(nodeDef, nodeConv);

                setNodeSanitization(nodeDef, nodeConv.getSanitization(), ms.getRuleSanitization());
                nodeDef.setMemoization(nodeConv.getMemoization());

                npe = new NodePearlElement();
                npe.add(nodeDef);
                nodeSection.add(npe);
            }
        }

        return nodeSection;
    }

    private void setNodeDefaultNamespace(NodeDefinition nodeDef, NodeConversion nodeConversion) {
        if (nodeConversion.getConverter() != null && nodeConversion.getConverter().getType().equals(RDFCapabilityType.uri) && nodeConversion.getDefaultNamespace() != null) {
            nodeDef.setDefaultNamespace(nodeConversion.getDefaultNamespace());
        }
    }

    /**
     * Set the flag for the sanitization annotations into the NodeDefinition.
     * For a node, a flag is set only if it differs from the rule configuration
     *
     * @param nodeDef
     * @param nodeSanitization
     * @param ruleSanitization
     */
    private void setNodeSanitization(NodeDefinition nodeDef, NodeSanitization nodeSanitization, NodeSanitization ruleSanitization) {
        NodeSanitization nodeDefSanitization = new NodeSanitization();

        // merge the sanitization of the rule with the CODA default as fallback, so the annotation are set whereas they differ
        ruleSanitization = NodeSanitization.merge(ruleSanitization, NodeSanitization.CODA_DEFAULT_SANITIZATION);

        if (nodeSanitization.getTrim() != null && !nodeSanitization.getTrim().equals(ruleSanitization.getTrim())) {
            //Trim specified and different from the one specified in rule
            nodeDefSanitization.setTrim(nodeSanitization.getTrim());
        }
        if (nodeSanitization.getRemoveDuplicateSpaces() != null && !nodeSanitization.getRemoveDuplicateSpaces().equals(ruleSanitization.getRemoveDuplicateSpaces())) {
            //RemoveDuplicateSpaces specified and different from the one specified in rule
            nodeDefSanitization.setRemoveDuplicateSpaces(nodeSanitization.getRemoveDuplicateSpaces());
        }
        //TODO check why it enter here
        if (nodeSanitization.getRemovePunctuation().getEnabled() != null && ( //remove punctuation specified for the node
                !nodeSanitization.getRemovePunctuation().getEnabled().equals(ruleSanitization.getRemovePunctuation().getEnabled()) || //different from the one specified for the rule
                (
                    nodeSanitization.getRemovePunctuation().getChars() != null && //chars specified
                    !nodeSanitization.getRemovePunctuation().getChars().equals(ruleSanitization.getRemovePunctuation().getChars()) //and different from those specified in the rule
                )
            )
        ) {
            //RemovePunctuation specified with status (enabled/disabled) or punctuation chars different from those specified in rule
            nodeDefSanitization.setRemovePunctuation(nodeSanitization.getRemovePunctuation());
        }
        if (nodeSanitization.getLowerCase() != null && !nodeSanitization.getLowerCase().equals(ruleSanitization.getLowerCase())) {
            //LowerCase specified and different from the one specified in rule
            nodeDefSanitization.setLowerCase(nodeSanitization.getLowerCase());
        }
        if (nodeSanitization.getUpperCase() != null && !nodeSanitization.getUpperCase().equals(ruleSanitization.getUpperCase())) {
            //UpperCase specified and different from the one specified in rule
            nodeDefSanitization.setUpperCase(nodeSanitization.getUpperCase());
        }
        nodeDef.setSanitization(nodeDefSanitization);
    }

    /**
     * @param ms
     * @return
     */
    private List<GraphPearlElement> buildGraphSection(MappingStruct ms) {
        List<GraphPearlElement> graphSection = new ArrayList<>();
        //subject
        {
            SubjectHeader subjHead = ms.getSubjectHeader();
            SimpleGraphApplication subjectGraphAppl = subjHead.getGraphApplication();
            if (subjectGraphAppl.getType() != null) {
                GraphPearlElementTripleBased gpe = new GraphPearlElementTripleBased(false, false);
                ValueOrString object = new ValueOrString(subjectGraphAppl.getType());
                gpe.add(subjectGraphAppl.getNodeId(), subjectGraphAppl.getProperty(), object);
                graphSection.add(gpe);
            }
            List<SimpleGraphApplication> additionalGraphs = subjHead.getAdditionalGraphApplications();
            for (SimpleGraphApplication ga : additionalGraphs) {
                GraphPearlElementTripleBased gpe = new GraphPearlElementTripleBased(false, false);
                gpe.add(ga.getNodeId(), ga.getProperty(), new ValueOrString(ga.getValue()));
                graphSection.add(gpe);
            }
        }
        //headers
        for (SimpleHeader h : ms.getHeaders()) {
            if (h.isIgnore()) continue;
            for (GraphApplication g : h.getGraphApplications()) {
                if (g instanceof SimpleGraphApplication) {
                    SimpleGraphApplication sga = (SimpleGraphApplication) g;
                    GraphPearlElementTripleBased gpe = new GraphPearlElementTripleBased(true, g.isDelete());
                    ValueOrString object = null;
                    if (sga.getNodeId() != null) {
                        object = new ValueOrString("$" + sga.getNodeId());
                    }
                    gpe.add(ms.getSubjectHeader().getGraphApplication().getNodeId(), sga.getProperty(), object);
                    graphSection.add(gpe);

                    if (sga.getType() != null) { //node assertion
                        gpe.add(sga.getNodeId(), RDF.TYPE, new ValueOrString(sga.getType()));
                    }
                } else if (g instanceof AdvancedGraphApplication) {
                    AdvancedGraphApplication aga = (AdvancedGraphApplication) g;
                    GraphPearlElementPatternBased gpe = new GraphPearlElementPatternBased(true, g.isDelete());

                    String graphPattern = aga.getPattern();
                    if (graphPattern.contains(AdvancedGraphApplication.PREDICATE_PLACEHOLDER)) {
                        IRI headerPredicate = h.getHeaderNameStruct().getPredicate();
                        if (headerPredicate != null) {
                            graphPattern = graphPattern.replace(AdvancedGraphApplication.PREDICATE_PLACEHOLDER, NTriplesUtil.toNTriplesString(headerPredicate));
                        } else { //header doesn't represent a predicate => use the default
                            if (aga.getDefaultPredicate() != null) {
                                graphPattern = graphPattern.replace(AdvancedGraphApplication.PREDICATE_PLACEHOLDER, NTriplesUtil.toNTriplesString(aga.getDefaultPredicate()));
                            } else { //no default predicate => write a placeholder like %pls_...%
                                graphPattern = graphPattern.replace(AdvancedGraphApplication.PREDICATE_PLACEHOLDER, "%pls_provide_a_predicate%");
                            }
                        }
                    }
                    gpe.setGraphPattern(graphPattern);
                    graphSection.add(gpe);
                }
            }
        }
        return graphSection;
    }

    private static class NodeConversionComparator implements Comparator<NodeConversion> {
        @Override
        public int compare(NodeConversion n1, NodeConversion n2) {
            if (n1.containsNodeReference()) return 1; //n1 contains reference to a node => must be serialized after
            if (n2.containsNodeReference())
                return -1; //n1 does not contain, but n2 does => n1 must be serialized before
            return 0; //none of the node conversion contains node reference, order is indifferent
        }

    }

}
