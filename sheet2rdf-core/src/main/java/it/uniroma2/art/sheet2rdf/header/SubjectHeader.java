package it.uniroma2.art.sheet2rdf.header;

import org.eclipse.rdf4j.model.vocabulary.RDF;

import java.util.ArrayList;
import java.util.List;

public class SubjectHeader extends Header {
	
	private NodeConversion nodeConversion;
	private SimpleGraphApplication graphApplication; //for subject type assertion
	private List<SimpleGraphApplication> additionalGraphApplications; //for additional subject p-o pairs
	
	public SubjectHeader() {
		nodeConversion = new NodeConversion();
		nodeConversion.setNodeId("subject");
		
		graphApplication = new SimpleGraphApplication();
		graphApplication.setNodeId(nodeConversion.getNodeId());
		graphApplication.setProperty(RDF.TYPE);

		additionalGraphApplications = new ArrayList<>();
	}

	public NodeConversion getNodeConversion() {
		return nodeConversion;
	}
	public void setNodeConversion(NodeConversion nodeConversion) {
		this.nodeConversion = nodeConversion;
	}
	
	public SimpleGraphApplication getGraphApplication() {
		return graphApplication;
	}
	public void setGraphApplication(SimpleGraphApplication graphApplication) {
		this.graphApplication = graphApplication;
	}

	public List<SimpleGraphApplication> getAdditionalGraphApplications() {
		return additionalGraphApplications;
	}
	public void setAdditionalGraphApplications(List<SimpleGraphApplication> additionalGraphApplications) {
		this.additionalGraphApplications = additionalGraphApplications;
	}
	
}
