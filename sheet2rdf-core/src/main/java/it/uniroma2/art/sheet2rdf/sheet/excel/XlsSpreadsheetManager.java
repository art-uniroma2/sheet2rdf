package it.uniroma2.art.sheet2rdf.sheet.excel;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.sheet.AbstractSpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class XlsSpreadsheetManager extends AbstractSpreadsheetManager {

    private static final Logger logger = LoggerFactory.getLogger(XlsSpreadsheetManager.class);

    public XlsSpreadsheetManager(File file) throws GenericSheet2RDFException {
        sheetsMap = new LinkedHashMap<>();
        prefixMappings = new HashMap<>();

        try (Workbook workbook = WorkbookFactory.create(file)) {
            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                Sheet s = workbook.getSheetAt(i);
                if (s.getSheetName().equals(PREFIX_MAPPING_SHEET_NAME)) {
                    this.initPrefixMapping(s);
                } else {
                    if (isSheetValid(s)) {
                        S2RDFSheet sheet = new S2RDFSheet(new XlsSheetManager(s));
                        sheetsMap.put(s.getSheetName(), sheet);
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Error", e);
        }
    }

    protected void initPrefixMapping(Sheet prefixMappingSheet) {
        prefixMappings = new HashMap<>();

        XlsSheetManager sheetMgr = new XlsSheetManager(prefixMappingSheet);
        int nRow = sheetMgr.getRowCount();
        for (int i = 0; i < nRow; i++) {
            String prefix = sheetMgr.getCellValue(i, 0);
            String namespace = sheetMgr.getCellValue(i, 1);
            if (!prefix.isEmpty() && namespace.isEmpty()) {
                prefixMappings.put(prefix, namespace);
            }
        }
    }

    /**
     * A sheet is valid if it's not empty and its content starts from the first cell of first row (header row not empty)
     * @param sheet
     * @return
     */
    private boolean isSheetValid(Sheet sheet) {
        Row row = sheet.getRow(0);
        if (row != null) {
            Cell cell = row.getCell(0);
            return cell != null && cell.getCellType() == CellType.STRING && !cell.getStringCellValue().isEmpty();
        }
        return false;
    }

}
