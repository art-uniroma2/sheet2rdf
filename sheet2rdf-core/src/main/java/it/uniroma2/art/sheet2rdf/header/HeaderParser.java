package it.uniroma2.art.sheet2rdf.header;

import java.net.URISyntaxException;
import java.util.Map;

import org.eclipse.rdf4j.common.net.ParsedIRI;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;

public class HeaderParser {
	
	/**
	 * Returns the header id, namely the name of the header without eventual language, datatype and 
	 * other info
	 */
	public static String parseId(String header) {
		if (header.contains("@")) { //remove language
			header = header.substring(0, header.indexOf("@"));
		}
		if (header.contains("^^")) { //remove datatype
			header = header.substring(0, header.indexOf("^^"));
		}
		if (header.contains("(")) { //remove other info
			header = header.substring(0, header.indexOf("("));
		}
		return header.trim(); 
	}

	/**
	 * Returns the language contained in the header. Returns null if no language is found
	 * @param header
	 * @return
	 */
	public static String parseLanguage(String header) {
		String lang = null;
		if (header.contains("@")) {
			lang = header.substring(header.lastIndexOf("@")+1);
			if (lang.contains("^^")) {
				lang = lang.substring(0, lang.indexOf("^^"));
			}
			if (lang.contains("(")) {
				lang = lang.substring(0, lang.indexOf("("));
			}
			lang = lang.trim();
		}
		return lang;
	}
	
	/**
	 * Returns the datatype contained in the header. Returns null if no datatype is found
	 * @param header
	 * @return
	 */
	public static IRI parseDatatype(String header, Map<String, String> prefixMapping) {
		IRI dt = null;
		if (header.contains("^^")) {
			header = header.substring(header.lastIndexOf("^^")+2);
			if (header.contains("(")) {
				header = header.substring(0, header.indexOf("("));
			}
			header = header.trim();
			
			if (header.startsWith("<http:")) { //iri
				header = header.substring(1, header.lastIndexOf(">"));
			} else { //qname?
				if (S2RDFUtils.isQName(header, prefixMapping)) {
					header = S2RDFUtils.expandQName(header, prefixMapping);
				}
			}
			try {
				dt = SimpleValueFactory.getInstance().createIRI(header);
			} catch (IllegalArgumentException e) {}
		}
		return dt;
	}
	
	/**
	 * Returns the resource contained in the header. Returns null if no resource has been parsed
	 * @param header
	 * @return
	 */
	public static IRI parseResource(String header, Map<String, String> prefixMapping) {
		IRI resource = null;
		header = HeaderParser.parseId(header);
		if (S2RDFUtils.isQName(header, prefixMapping)) {
			header = S2RDFUtils.expandQName(header, prefixMapping);
		}
		if (header.startsWith("http:")) { //iri
			try {
				/* the following ParsedIRI initialization ensures that the header string represents an IRI which
				can be used in SPARQL query. Otherwise, the sole createIRI of SimpleValueFactory could bring to a
				MalformedQueryException when the same IRI is used in a SPARQL query (e.g. in isClass).
				I saw it is used in AbstractRDFParser.createURI() which is used when loading data.
				 */
				new ParsedIRI(header);
				resource = SimpleValueFactory.getInstance().createIRI(header);
			} catch (IllegalArgumentException | URISyntaxException e) {}
		}
		return resource;
	}
	
	
}
