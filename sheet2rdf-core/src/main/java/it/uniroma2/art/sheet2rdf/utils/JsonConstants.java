package it.uniroma2.art.sheet2rdf.utils;

public class JsonConstants {

	public static final String MAPPING_STRUCT_SUBJECT = "subject";
	public static final String MAPPING_STRUCT_HEADERS = "headers";
	public static final String MAPPING_STRUCT_HEADER_FS_MAP = "headerFsMap";
	public static final String MAPPING_STRUCT_RULE_SANITIZATION = "ruleSanitization";
	
	public static final String SUBJECT_HEADER_ID = "id";
	public static final String SUBJECT_HEADER_PEARL_FEATURE = "pearlFeature";
	public static final String SUBJECT_HEADER_NODE = "node";
	public static final String SUBJECT_HEADER_GRAPH = "graph";
	public static final String SUBJECT_HEADER_ADDITIONAL_GRAPHS = "additionalGraphs";
	
	public static final String SIMPLE_HEADER_ID = "id";
	public static final String SIMPLE_HEADER_PEARL_FEATURE = "pearlFeature";
	public static final String SIMPLE_HEADER_NAME_STRUCT = "nameStruct";
	public static final String SIMPLE_HEADER_NAME = "name";
	public static final String SIMPLE_HEADER_FULL_NAME = "fullName";
	public static final String SIMPLE_HEADER_LANG = "lang";
	public static final String SIMPLE_HEADER_DATATYPE = "datatype";
	public static final String SIMPLE_HEADER_COL_INDEX = "columnIdx";
	public static final String SIMPLE_HEADER_NODES = "nodes";
	public static final String SIMPLE_HEADER_GRAPH = "graph";
	public static final String SIMPLE_HEADER_IS_MULTIPLE = "isMultiple";
	public static final String SIMPLE_HEADER_IGNORE = "ignore";

	public static final String CONVERTER_TYPE = "type";
	public static final String CONVERTER_CONTRACT_URI = "contractUri";
	public static final String CONVERTER_LANGUAGE = "language";
	public static final String CONVERTER_DATATYPE_URI = "datatypeUri";
	public static final String CONVERTER_PARAMS = "params";

	public static final String GRAPH_APPLICATION_ID = "id";
	public static final String GRAPH_APPLICATION_DELETE = "delete";
	
	public static final String SIMPLE_GRAPH_APPLICATION_PROPERTY = "property";
	public static final String SIMPLE_GRAPH_APPLICATION_NODE_ID = "nodeId";
	public static final String SIMPLE_GRAPH_APPLICATION_TYPE = "type";
	public static final String SIMPLE_GRAPH_APPLICATION_VALUE = "value";
	
	public static final String ADVANCED_GRAPH_APPLICATION_NODE_IDS = "nodeIds";
	public static final String ADVANCED_GRAPH_APPLICATION_PATTERN = "pattern";
	public static final String ADVANCED_GRAPH_APPLICATION_PREFIX_MAPPING = "prefixMapping";
	public static final String ADVANCED_GRAPH_APPLICATION_DEFAULT_PREDICATE = "defaultPredicate";
	
	
	
}
