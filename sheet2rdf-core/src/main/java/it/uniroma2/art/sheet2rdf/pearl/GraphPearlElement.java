package it.uniroma2.art.sheet2rdf.pearl;

public abstract class GraphPearlElement implements PearlElement {
	
	protected boolean optional;
	protected boolean delete;
	
	public GraphPearlElement(boolean optional, boolean delete) {
		this.optional = optional;
		this.delete = delete;
	}

	public boolean isDelete() {
		return delete;
	}
}
