package it.uniroma2.art.sheet2rdf.pearl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;

import it.uniroma2.art.coda.structures.ValueOrString;
import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class GraphPearlElementTripleBased extends GraphPearlElement {

	private List<GraphTriple> triplesList;

	public GraphPearlElementTripleBased(boolean optional, boolean delete) {
		super(optional, delete);
		triplesList = new ArrayList<>();
	}

	/**
	 * Adds a graph triple to the GraphPearlElement
	 *
	 * @param subject
	 * @param predicate if null, the serialization will replace the predicate with a placeholder like
	 *                  &lt;please_provide_a_predicate&gt;
	 * @param object
	 */
	public void add(String subject, IRI predicate, ValueOrString object) {
		subject = "$" + subject;
		triplesList.add(new GraphTriple(subject, predicate, object));
	}

	@Override
	public String serialize(String tabs, Map<String, String> prefixMapping) {
		List<String> triplesSerialized = new ArrayList<>();
		for (GraphTriple triple : triplesList) {
			String subject = triple.getSubject();
			String predicateSerialization = "%pls_provide_a_predicate%";
			if (triple.getPredicate() != null) {
				String qname = S2RDFUtils.asQName(triple.getPredicate(), prefixMapping);
				if (qname == null) { //not been collapsed to qname
					predicateSerialization = NTriplesUtil.toNTriplesString(triple.getPredicate());
				} else {
					predicateSerialization = qname;
				}
			}
			String objectSerialization = "";
			ValueOrString object = triple.getObject();
			if (object == null) {
				objectSerialization = "%pls_provide_a_value%";
			} else if (object.isValue()) {
				Value objValue = object.getValue();
				if (objValue instanceof IRI) {
					//try to get the IRI as qname
					String qname = S2RDFUtils.asQName((IRI) objValue, prefixMapping);
					if (qname != null) {
						objectSerialization = qname;
					} else {
						objectSerialization = NTriplesUtil.toNTriplesString(objValue);
					}
				}
			} else {
				objectSerialization = object.getString();
			}
			triplesSerialized.add(subject + " " + predicateSerialization + " " + objectSerialization + " .");
		}
		String output;
		if (triplesSerialized.size() == 1) {
			if (optional) {
				output = tabs + "OPTIONAL { " + triplesSerialized.get(0) + " }";
			} else {
				output = tabs + triplesSerialized.get(0);
			}
		} else { //multiple triple
			if (optional) {
				String optionalBlockSerialized = "";
				for (String ts : triplesSerialized) {
					optionalBlockSerialized += "\n" + tabs + "\t" + ts; //optional, so newline and a further tab
				}
				output = tabs + "OPTIONAL {" + optionalBlockSerialized + "\n" + tabs + "}";
			} else {
				output = String.join("\n" + tabs, triplesSerialized);
			}
		}
		return output;
	}


	private static class GraphTriple {

		private String subject;
		private IRI predicate;
		private ValueOrString object;

		public GraphTriple(String subject, IRI predicate, ValueOrString object) {
			this.subject = subject;
			this.predicate = predicate;
			this.object = object;
		}

		public String getSubject() {
			return subject;
		}

		public IRI getPredicate() {
			return predicate;
		}

		public ValueOrString getObject() {
			return object;
		}
	}

}
