package it.uniroma2.art.sheet2rdf.sheet;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;

import java.util.List;

public interface SheetManager {

    /**
     * Returns the name of the sheet
     * @return
     */
    String getSheetName();

    /**
     * Returns a list of the headers of the excel data sheet or the header in the table of the DB
     * @param includeDuplicate the returned list contains eventual multiple headers
     * @return
     */
    List<String> getHeaders(boolean includeDuplicate) throws GenericSheet2RDFException;

    /**
     * Return a table containing all the value in the datasheet (except the headers) or in the table of the DB.
     * @return
     */
    List<List<String>> getDataTable() throws GenericSheet2RDFException;

    /**
     * Return the string value of the cell in a specified row and column (not usable for the table of the DB)
     * @param row
     * @param column
     * @return
     */
    String getCellValue(int row, int column);


    /**
     * Checks if in headers row there are more consecutive cell with the same value (headerValue). For the table of the
     * DB it always return false, since in a DB table it cannot exist multiple column with the same header
     *
     * @param headerValue
     * @param headerValue
     * @return
     */
    boolean isMultipleHeader(String headerValue);


    /**
     * Returns number of columns of the datasheet or the number of columns in the table of the DB
     * @return
     */
    int getColumnCount() throws GenericSheet2RDFException;

    /**
     * Returns number of rows of the data sheet
     * @return
     */
    int getRowCount() throws GenericSheet2RDFException;

}
