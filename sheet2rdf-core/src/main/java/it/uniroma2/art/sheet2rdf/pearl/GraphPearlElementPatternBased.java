package it.uniroma2.art.sheet2rdf.pearl;

import java.util.Map;

public class GraphPearlElementPatternBased extends GraphPearlElement {
	
	private String graphPattern;
	
	public GraphPearlElementPatternBased(boolean optional, boolean delete) {
		super(optional, delete);
	}
	
	public void setGraphPattern(String graphPattern) {
		this.graphPattern = graphPattern;
	}

	@Override
	public String serialize(String tabs, Map<String, String> prefixMapping) {
		String output = graphPattern;
		if (optional){
			output = output.replace("\n", "\n\t" + tabs);
			output = output.replace("(\r\n|\n)", "$1\t" + tabs);
			output = tabs + "OPTIONAL {\n\t" + tabs + output + "\n" + tabs + "}";
		}
		return output;
	}

}
