package it.uniroma2.art.sheet2rdf.sheet;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;

import java.util.List;

public class S2RDFSheet {

    private final String name;
    private final List<String> headers;
    private final List<List<String>> tableContent;

    public S2RDFSheet(SheetManager sheetManager) throws GenericSheet2RDFException {
        this.name = sheetManager.getSheetName();
        this.headers = sheetManager.getHeaders(true);
        this.tableContent = sheetManager.getDataTable();
    }

    public String getName() {
        return name;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public List<List<String>> getTableContent() {
        return tableContent;
    }

    public String getCellValue(int row, int col) {
        return this.tableContent.get(row).get(col);
    }

    public int getRowCount() {
        return this.tableContent.size();
    }

    public int getColumnCount() {
        return this.headers.size();
    }


}
