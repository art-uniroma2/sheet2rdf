package it.uniroma2.art.sheet2rdf.header;

import java.util.Map;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import it.uniroma2.art.sheet2rdf.utils.RDF4JValueSerializer;
import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;

public class HeaderNameStruct {
	
	private String fullName; //entire header name
	private String name; //header name stripped from eventual language or datatype
	private IRI predicate; //predicate that the "name" represent (if any)
	private IRI cls; //cls that the "name" represent (if any)
	private String lang; //language included in the header name (if any). e.g. "my prop@en"
	private IRI datatype; //datatype included in the header name (if any). e.g. "my prop^^xsd:string"
	
	/**
	 * @param headerName name of the header, used in order to detect language, datatype and if it represents a resource (class or property)
	 * @param connection repo connection, needed for checking property or class represented by the header
	 * @param prefixMapping
	 */
	public HeaderNameStruct(String headerName, RepositoryConnection connection, Map<String, String> prefixMapping) {
		this.fullName = headerName;
		this.name = HeaderParser.parseId(headerName);
		this.lang = HeaderParser.parseLanguage(headerName);
		this.datatype = HeaderParser.parseDatatype(headerName, prefixMapping);
		
		IRI headerRes = HeaderParser.parseResource(this.name, prefixMapping);
		if (headerRes != null) {
			if (S2RDFUtils.isClass(headerRes, connection)) { //header resource is a class
				this.cls = headerRes;
			} else if (S2RDFUtils.isProperty(headerRes, connection)){ //header resource is a property
				this.predicate = headerRes;
			}
		}
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String completeName) {
		this.fullName = completeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public IRI getPredicate() {
		return predicate;
	}

	public void setPedicate(IRI predicate) {
		this.predicate = predicate;
	}

	public IRI getCls() {
		return cls;
	}

	public void setCls(IRI cls) {
		this.cls = cls;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public IRI getDatatype() {
		return datatype;
	}

	public void setDatatype(IRI datatype) {
		this.datatype = datatype;
	}
	
	public String toString() {
		String s = "";
		s += "fullName: " + this.fullName;
		s += "; name: " + this.name;
		s += "; lang: " + this.lang;
		s += "; dt: " + this.datatype;
		return s;
	}
	
	public JsonNode toJson() {
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addSerializer(IRI.class, new RDF4JValueSerializer());
		mapper.registerModule(module);
		return mapper.valueToTree(this);
	}
	
}
