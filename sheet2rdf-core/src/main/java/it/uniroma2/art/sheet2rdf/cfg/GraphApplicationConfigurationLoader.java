package it.uniroma2.art.sheet2rdf.cfg;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import it.uniroma2.art.sheet2rdf.header.AdvancedGraphApplication;
import it.uniroma2.art.sheet2rdf.header.NodeConversion;
import it.uniroma2.art.sheet2rdf.header.SimpleHeader;
import org.apache.poi.util.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class GraphApplicationConfigurationLoader {
	
	public static final String XLABEL_CONFIGURATION_FILE = "xlabel.cfg";
	
	public static List<String> getAvailableConfigurations() {
		return List.of("xlabel");
	}

	/**
	 * Returns the configuration file with the given name
	 * @param fileName
	 * @return
	 * @throws IOException 
	 * @throws  
	 */
	public static File getConfigurationFile(String fileName) throws IOException {
		InputStream is = GraphApplicationConfigurationLoader.class.getResourceAsStream(fileName);
		File cfgFile = File.createTempFile("configFile", ".cfg");
		try (OutputStream os = new FileOutputStream(cfgFile)) {
			IOUtils.copy(is, os);
		}
		return cfgFile;
	}
	
	/**
	 * Load the given configuration file (written in YAML format) into a JSON object
	 * @param cfgFile
	 * @return
	 * @throws IOException
	 */
	public static ObjectNode loadConfigurationFromYAMLFiles(File cfgFile) throws IOException {
		YAMLFactory fact = new YAMLFactory();
		fact.enable(YAMLGenerator.Feature.MINIMIZE_QUOTES);
		fact.disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER);
		ObjectMapper mapper = new ObjectMapper(fact);
		ObjectReader objReader = mapper.reader();
		ObjectNode obj = mapper.createObjectNode();

		try (Reader reader = new InputStreamReader(new FileInputStream(cfgFile), StandardCharsets.UTF_8)) {
			JsonNode jsonNode = objReader.readTree(reader);
			if (jsonNode != null) {
				if (!(jsonNode instanceof ObjectNode)) {
					throw new IllegalArgumentException("YAML file not cotaining an object node: " + cfgFile);
				}
				obj.setAll((ObjectNode) jsonNode);
			}
		} catch (JsonMappingException e) {
			// Swallow exception due to empty property files
			if (!(e.getPath().isEmpty() && e.getMessage().contains("end-of-input"))) {
				throw e;
			}
		}
		return obj;
	}
	
	/**
	 * Load the nodes and graph of a configuration into an header
	 * @param header SimpleHeader that will be configured
	 * @param cfgFileName name of the configuration file (local to Sheet2RDF)
	 * @param headerPlc placeholder of the header, used as prefix for appending the node Ids used in the configuration 
	 * @throws IOException
	 */
	public static void loadConfigurationInHeader(SimpleHeader header, String cfgFileName, String headerPlc) throws IOException {
		File xlabelCfgFile = getConfigurationFile(cfgFileName);
		ObjectNode cfgJson = loadConfigurationFromYAMLFiles(xlabelCfgFile);
		
		GraphApplicationConfigurationParser parser = new GraphApplicationConfigurationParser(cfgJson, headerPlc);
		
		List<NodeConversion> nodes = parser.getNodeConversions();
		/*
		 * If the header has a language specified and a node conversion use the literal converter with a language tag 
		 * replace the latter with the header language
		 */
		String headerLang = header.getHeaderNameStruct().getLang();
		if (headerLang != null) {
			for (NodeConversion n: nodes) {
				//here is it safety to skip null check on getConverter() since the configuration provides surely a converter for each node
				if (n.getConverter().getLanguage() != null) {
					n.getConverter().setLanguage(headerLang);
				}
			}
		}
		header.setNodeConversions(nodes);
		
		AdvancedGraphApplication graphApplication = parser.getAdvancedGraphApplication(nodes, header.getHeaderNameStruct().getPredicate());
		header.addGraphApplication(graphApplication);
	}
	
}
