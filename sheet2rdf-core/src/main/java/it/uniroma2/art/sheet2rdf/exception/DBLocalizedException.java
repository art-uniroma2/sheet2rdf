package it.uniroma2.art.sheet2rdf.exception;

public class DBLocalizedException extends GenericSheet2RDFException {

    public DBLocalizedException(Exception e) {
        super(e);
    }
}
