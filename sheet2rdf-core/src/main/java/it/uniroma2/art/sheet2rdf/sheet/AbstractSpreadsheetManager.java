package it.uniroma2.art.sheet2rdf.sheet;

import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractSpreadsheetManager implements SpreadsheetManager {

    protected Map<String, S2RDFSheet> sheetsMap; //name -> sheet
    protected Map<String, String> prefixMappings;

    @Override
    public Map<String, String> getPrefixNamespaceMapping() {
        return this.prefixMappings;
    }

    @Override
    public List<S2RDFSheet> getSheets() {
        return new ArrayList<>(sheetsMap.values());
    }

    @Override
    public S2RDFSheet getSheet(String sheetName) throws NotExistingSheetException {
        S2RDFSheet sheet = sheetsMap.get(sheetName);
        if (sheet == null) {
            throw new NotExistingSheetException("Cannot find a sheet with name " + sheetName);
        }
        return sheet;
    }

    @Override
    public S2RDFSheet getSheet(int index) {
        return this.getSheets().get(index);
    }

    @Override
    public int getDataSheetsCount() {
        return sheetsMap.size();
    }

}
