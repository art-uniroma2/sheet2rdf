package it.uniroma2.art.sheet2rdf.coda;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.ProjectionRuleModelNotSet;
import it.uniroma2.art.coda.exception.ValueNotPresentDueToConfigurationException;
import it.uniroma2.art.coda.exception.parserexception.PRParserException;
import it.uniroma2.art.coda.exception.RDFModelNotSetException;
import it.uniroma2.art.coda.exception.UnassignableFeaturePathException;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.jcas.JCas;
import org.eclipse.rdf4j.repository.RepositoryConnection;

public class Sheet2RDFCODA {
	
	private CODACore codaCore;
	
	public Sheet2RDFCODA(RepositoryConnection repoConnection, CODACore codaCore){
		this.codaCore = codaCore;
		try {
			codaCore.initialize(repoConnection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<SuggOntologyCoda> suggestTriples(JCas jcas, File pearlFile) throws PRParserException,
			ComponentProvisioningException, ConverterException, DependencyException,
			RDFModelNotSetException, ProjectionRuleModelNotSet, UnassignableFeaturePathException,
			ValueNotPresentDueToConfigurationException {
		List<SuggOntologyCoda> listSuggOntCoda = new ArrayList<SuggOntologyCoda>();
		codaCore.setProjectionRulesModel(pearlFile);	
		codaCore.setJCas(jcas, false);
		while (codaCore.isAnotherAnnotationPresent()){
			SuggOntologyCoda suggOntCoda = codaCore.processNextAnnotation();
			if (suggOntCoda.getAnnotation().getType().getName().startsWith("it.uniroma2"))//returns only relevant annotations
				listSuggOntCoda.add(suggOntCoda);
		}
		return listSuggOntCoda;
	}

}
