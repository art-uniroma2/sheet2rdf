package it.uniroma2.art.sheet2rdf.core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.uniroma2.art.coda.converters.contracts.RandomIdGenerator;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.sheet2rdf.cfg.GraphApplicationConfigurationLoader;
import it.uniroma2.art.sheet2rdf.coda.CODAConverter;
import it.uniroma2.art.sheet2rdf.header.AdvancedGraphApplication;
import it.uniroma2.art.sheet2rdf.header.GraphApplication;
import it.uniroma2.art.sheet2rdf.header.Header;
import it.uniroma2.art.sheet2rdf.header.HeaderNameStruct;
import it.uniroma2.art.sheet2rdf.header.NodeConversion;
import it.uniroma2.art.sheet2rdf.header.NodeSanitization;
import it.uniroma2.art.sheet2rdf.header.SimpleGraphApplication;
import it.uniroma2.art.sheet2rdf.header.SimpleHeader;
import it.uniroma2.art.sheet2rdf.header.SubjectHeader;
import it.uniroma2.art.sheet2rdf.resolver.ConverterResolver;
import it.uniroma2.art.sheet2rdf.resolver.PropertyRangeResolver;
import it.uniroma2.art.sheet2rdf.utils.FsNamingStrategy;
import it.uniroma2.art.sheet2rdf.utils.JsonConstants;
import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class MappingStruct {

	private final List<SimpleHeader> headers;
	private SubjectHeader subjHeader;
	private Map<String, String> headerFsMap;
	private NodeSanitization ruleSanitization;

	private final PropertyRangeResolver rangeResolver;

	private final Map<String, String> prefixMapping;

	public MappingStruct(List<String> headerList, RepositoryConnection connection, Map<String, String> prefixMapping) {
		this(headerList, connection, prefixMapping, FsNamingStrategy.columnNumericIndex);
	}
	
	public MappingStruct(List<String> headerList, RepositoryConnection connection, Map<String, String> prefixMapping, FsNamingStrategy fsStrategy) {
		headers = new ArrayList<>();
		subjHeader = new SubjectHeader();
		headerFsMap = new HashMap<>();
		this.prefixMapping = prefixMapping;
		ruleSanitization = new NodeSanitization();

		rangeResolver = new PropertyRangeResolver(connection);
		
		for (int i = 0; i < headerList.size(); i++){
			String headerName = headerList.get(i);
			HeaderNameStruct hns = new HeaderNameStruct(headerName, connection, prefixMapping);
			SimpleHeader header = new SimpleHeader(hns, i);
			headerFsMap.put(header.getId(), generateFsName(headerName, i, fsStrategy));
			enrichHeader(header);
			this.headers.add(header);
		}
	}
	
	/**
	 * Initializes (where possible) the header with node conversions and graph applications
	 * by inferring information from the header name
	 * @param header
	 */
	private void enrichHeader(SimpleHeader header) {
		//init a simple conversion
		
		IRI headerCls = header.getHeaderNameStruct().getCls();
		IRI headerProp = header.getHeaderNameStruct().getPredicate();
		if (headerCls != null) { //header resource is a class => this header represents the subject
			subjHeader.setId(header.getId()); //override the empty subj
			subjHeader.getNodeConversion().setConverter(ConverterResolver.getConverter(headerCls));
			subjHeader.getGraphApplication().setType(headerCls);
		} else if (headerProp != null) { //header resource is a property
			//create a conversion for the given property
			
			//special case for initializing range type and range class for skosxl:pref/alt/hiddenLabel
			if (headerProp.equals(SKOSXL.PREF_LABEL) || headerProp.equals(SKOSXL.ALT_LABEL) || headerProp.equals(SKOSXL.HIDDEN_LABEL)) {
				//generate the default advanced conversion for skosxl label
				try {
					GraphApplicationConfigurationLoader.loadConfigurationInHeader(header, 
							GraphApplicationConfigurationLoader.XLABEL_CONFIGURATION_FILE,
							getFeatureStructName(header));
				} catch (IOException e) {
					//failed to load the advanced conversion from the configuration file => do it manually
					/*
					 * Node conversions
					 */
					//literal form
					NodeConversion litFormNode = new NodeConversion();
					litFormNode.setNodeId(getFeatureStructName(header)+"_litForm_node");
					CODAConverter converter = new CODAConverter(RDFCapabilityType.literal);
					String headerLang = header.getHeaderNameStruct().getLang();
					if (headerLang != null) {
						converter.setLanguage(headerLang);
					}
					litFormNode.setConverter(converter);
					header.addNodeConversions(litFormNode);
					//xlabel uri
					NodeConversion xlabelNode = new NodeConversion();
					xlabelNode.setNodeId(getFeatureStructName(header)+"_xlabelUri_node");
					converter = new CODAConverter(RDFCapabilityType.uri, RandomIdGenerator.CONTRACT_URI);
					converter.addParam("xRole", RandomIdGenerator.XRoles.XLABEL);
					Map<String, String> argsMap = new LinkedHashMap<>();
					argsMap.put("lexicalForm", litFormNode.getNodeId());
					converter.addParam("args", argsMap);
					xlabelNode.setConverter(converter);
					header.addNodeConversions(xlabelNode);
					/*
					 * Graph Application
					 */
					AdvancedGraphApplication ga = new AdvancedGraphApplication();
					ga.setNodeIds(Arrays.asList(litFormNode.getNodeId(), xlabelNode.getNodeId()));
					String graphPattern = "$subject " + 
							headerProp.stringValue().replace(SKOSXL.NAMESPACE, SKOSXL.PREFIX + ":") + " " + 
							"$" + xlabelNode.getNodeId() + ".\n" +
							"$" + xlabelNode.getNodeId() + " " +
							SKOSXL.LITERAL_FORM.stringValue().replace(SKOSXL.NAMESPACE, SKOSXL.PREFIX + ":") + " " +
							"$" + litFormNode.getNodeId() + "."; 
					ga.setPattern(graphPattern);
					//add skosxl to the prefix mapping
					Map<String, String> prefMap = new HashMap<>();
					prefMap.put(SKOSXL.PREFIX, SKOSXL.NAMESPACE);
					ga.setPrefixMapping(prefMap);
					
					header.addGraphApplication(ga);
				}
			} else { //properties for which doesn't exist a configuration (any properties different from skosxl:pref/alt/hiddenLabel
				/*
				 * Node conversion
				 */
				NodeConversion c = new NodeConversion();
				c.setNodeId(getFeatureStructName(header)+"_node");
				
				CODAConverter converter = null;
				IRI rangeCls = rangeResolver.getRange(headerProp);
				String headerLang = header.getHeaderNameStruct().getLang();
				IRI headerDatatype = header.getHeaderNameStruct().getDatatype();
				if (rangeCls != null) {
					converter = ConverterResolver.getConverter(rangeCls);
				} else if (headerLang != null || headerDatatype != null) {
					//no range class to determine the converter, but language or datatype provided, set the literal converter
					converter = new CODAConverter(RDFCapabilityType.literal);
				}
				if (converter != null && converter.getType() == RDFCapabilityType.literal) {
					converter.setLanguage(headerLang);
					if (headerLang == null) {
						//datatype set only if language not provided (prevent cases like literal@en^^xsd:langString
						if (headerDatatype != null) {
							converter.setDatatypeUri(headerDatatype.stringValue());
						}
					}
				}
				c.setConverter(converter);
				
				header.addNodeConversions(c);
				
				/*
				 * Graph application
				 */
				
				SimpleGraphApplication ga = new SimpleGraphApplication();
				ga.setProperty(headerProp);
				ga.setNodeId(c.getNodeId());
				header.addGraphApplication(ga);
			}
		} else { 
			/*
			 * header name doesn't represent a resource (neither a class nor a property),
			 * creates node conversion and graph application trying to infer some info anyway (like lang and datatype)
			 */
			String headerLang = header.getHeaderNameStruct().getLang();
			IRI headerDatatype = header.getHeaderNameStruct().getDatatype();
			if (headerLang != null || headerDatatype != null) {
				NodeConversion c = new NodeConversion();
				c.setNodeId(getFeatureStructName(header)+"_node");
				CODAConverter converter;
				//no range class to determine the converter, but language or datatype provided, set the literal converter
				converter = new CODAConverter(RDFCapabilityType.literal);
				converter.setLanguage(headerLang);
				if (headerLang == null) {
					//datatype set only if language not provided (prevent cases like literal@en^^xsd:langString
					converter.setDatatypeUri(headerDatatype.stringValue());
				}
				c.setConverter(converter);
				header.addNodeConversions(c);
			}
			
		}
	}
	
	/**
	 * Replicate the given header configuration to all the headers with the same name.
	 * Currently, for semplicity, this method considers only the headers with the same identical name,
	 * comprehensive of the language tag or the datatype.
	 * @param sourceHeader
	 */
	public void replicateMultipleHeader(SimpleHeader sourceHeader) {
		for (SimpleHeader targetHeader: this.headers) {
			if (targetHeader.getId().equals(sourceHeader.getId())) continue; //skip the same source header
			if (sourceHeader.hasSameName(targetHeader)) { //same header name => copy configuration
				
				Map<String, String> nodeIdReplacementMap = new HashMap<>(); //map sourceId-targetId, useful to replace eventual node id in the graph pattern
				
				//node conversions
				List<NodeConversion> sourceNodes = sourceHeader.getNodeConversions();
				List<NodeConversion> targetNodes = new ArrayList<>();
				
				for (NodeConversion sourceNode: sourceNodes) {
					NodeConversion targetNode = new NodeConversion();
					//id
					String replicatedId = getReplicatedNodeId(sourceNode.getNodeId(), sourceHeader, targetHeader);
					targetNode.setNodeId(replicatedId);
					nodeIdReplacementMap.put(sourceNode.getNodeId(), targetNode.getNodeId());
					//converter
					CODAConverter sourceConverter = sourceNode.getConverter();
					if (sourceConverter != null) {
						CODAConverter targetConverter = new CODAConverter();
						targetConverter.setType(sourceConverter.getType());
						targetConverter.setContractUri(sourceConverter.getContractUri());
						String targetLang = sourceConverter.getLanguage(); //by default set the same lang of the source header
						//if the target headers has a different language provided in the name, set its lang
						if (
							targetHeader.getHeaderNameStruct().getLang() != null &&
							!targetHeader.getHeaderNameStruct().getLang().equals(sourceHeader.getHeaderNameStruct().getLang())
						){
							targetLang = targetHeader.getHeaderNameStruct().getLang();
						}
						//set the language only if the converter is literal
						if (targetConverter.getType() == RDFCapabilityType.literal) {
							targetConverter.setLanguage(targetLang);
						}
						
	
						String targetDt = sourceConverter.getDatatypeUri(); //by default set the same datatype of the source header
						//if the target headers has a different datatype provided in the name, set its datatype
						if (
							targetHeader.getHeaderNameStruct().getDatatype() != null &&
							!targetHeader.getHeaderNameStruct().getDatatype().equals(sourceHeader.getHeaderNameStruct().getDatatype())
						){
							targetDt = targetHeader.getHeaderNameStruct().getDatatype().stringValue();
						}
						targetConverter.setDatatypeUri(targetDt);
						//set the same params of the source converter. The node id references will be replaced later
						targetConverter.setParams(sourceConverter.getParams());
						targetNode.setConverter(targetConverter);
					}

					targetNode.setMemoization(sourceNode.getMemoization());
					targetNode.setSanitization(sourceNode.getSanitization());

					targetNodes.add(targetNode);
				}
				//parameters of node converters replaced after each node has been copied, so the nodeIdReplacementMap is complete
				for (NodeConversion n: targetNodes) {
					CODAConverter converter = n.getConverter();
					if (converter != null) {
						Map<String, Object> replacedParamMap = S2RDFUtils.replaceNodesIdInConverterParams(converter.getParams(), nodeIdReplacementMap);
						converter.setParams(replacedParamMap);
					}
				}
				targetHeader.setNodeConversions(targetNodes);
				//graph applications
				List<GraphApplication> sourceGraphs = sourceHeader.getGraphApplications();
				List<GraphApplication> targetGraphs = new ArrayList<>();
				for (GraphApplication sourceGraph: sourceGraphs) {
					if (sourceGraph instanceof SimpleGraphApplication) {
						SimpleGraphApplication targetGraph = new SimpleGraphApplication();
						targetGraph.setNodeId(nodeIdReplacementMap.get(((SimpleGraphApplication) sourceGraph).getNodeId()));
						targetGraph.setProperty(((SimpleGraphApplication) sourceGraph).getProperty());
						targetGraph.setType(((SimpleGraphApplication) sourceGraph).getType());
						targetGraphs.add(targetGraph);
					} else if (sourceGraph instanceof AdvancedGraphApplication) {
						AdvancedGraphApplication targetGraph = new AdvancedGraphApplication();
						//replicate nodes
						List<String> sourceNodeIds = ((AdvancedGraphApplication) sourceGraph).getNodeIds();
						List<String> targetNodeIds = new ArrayList<>();
						for (String sourceId: sourceNodeIds) {
							targetNodeIds.add(nodeIdReplacementMap.get(sourceId));
						}
						targetGraph.setNodeIds(targetNodeIds);
						//replicate pattern
						String targetPattern = ((AdvancedGraphApplication) sourceGraph).getPattern();
						for (Entry<String, String> replacement: nodeIdReplacementMap.entrySet()) {
							targetPattern = targetPattern.replace("$"+replacement.getKey(), "$"+replacement.getValue());
						}
						targetGraph.setPattern(targetPattern);
						//replicate prefix mapping
						targetGraph.setPrefixMapping(((AdvancedGraphApplication) sourceGraph).getPrefixMapping());
						//replicate default predicate
						targetGraph.setDefaultPredicate(((AdvancedGraphApplication) sourceGraph).getDefaultPredicate());
						
						targetGraphs.add(targetGraph);
					}
				}
				targetHeader.setGraphApplications(targetGraphs);
				targetHeader.setIgnore(sourceHeader.isIgnore());
			}
		}
	}
	
	private String getReplicatedNodeId(String sourceNodeId, SimpleHeader sourceHeader, SimpleHeader targetHeader) {
		String replicatedId = sourceNodeId;
		if (replicatedId.contains(getFeatureStructName(sourceHeader))) {
			//if the node ID contains the fsName of the header, replace it with the target header fs name
			replicatedId = replicatedId.replace(getFeatureStructName(sourceHeader), getFeatureStructName(targetHeader));
		} else { //otherwise, add the target header fs name as prefix
			replicatedId = getFeatureStructName(targetHeader) + "_" + replicatedId;
		}
		return replicatedId;
	}

	public Map<String, String> getPrefixMapping() {
		return prefixMapping;
	}

	public List<SimpleHeader> getHeaders() {
		return headers;
	}
	
	public SubjectHeader getSubjectHeader() {
		return subjHeader;
	}
	public void setSubjectHeader(SubjectHeader header) {
		subjHeader = header;
	}

	public Map<String, String> getHeaderFsMap() {
		return headerFsMap;
	}
	public void setHeaderFsMap(Map<String, String> headerFsMap) {
		this.headerFsMap = headerFsMap;
	}

	public NodeSanitization getRuleSanitization() {
		return ruleSanitization;
	}
	public void setRuleSanitization(NodeSanitization ruleSanitization) {
		this.ruleSanitization = ruleSanitization;
	}

	public String getFeatureStructName(Header header) {
		return headerFsMap.get(header.getId());
	}
	public String getFeatureStructureName(String headerId) {
		return this.headerFsMap.get(headerId);
	}
	public List<String> convertHeaderFsMapToList() {
		List<String> asList = new ArrayList<>();
		for (String headerId: this.headerFsMap.keySet()) {
			asList.add(headerId);
			asList.add(this.headerFsMap.get(headerId));
		}
		return asList;
	}
	
	
	private String generateFsName(String headerName, int columnIdx, FsNamingStrategy strategy) {
		if (strategy == FsNamingStrategy.columnAlphabeticIndex) {
			return "col_" + toAlphabetic(columnIdx);
		} else if (strategy == FsNamingStrategy.columnNumericIndex) {
			return "col_" + columnIdx;
		} else { //normalizedHeaderName
			return "col_" + columnIdx + "_" + normalizeHeader(headerName);
		}
	}
	
	private String normalizeHeader(String headerName) {
		//replace with _ everything that is not letters (upper/lower case) or digits
		String normalized = headerName.replaceAll("[^a-zA-Z0-9]", "_");
		normalized = normalized.replaceAll("_+", "_");
		return normalized;
	}
	
	private String toAlphabetic(int i) {
	    int quot = i/26;
	    int rem = i%26;
	    char letter = (char)((int)'A' + rem);
	    if( quot == 0 ) {
	        return ""+letter;
	    } else {
	        return toAlphabetic(quot-1) + letter;
	    }
	}

	public SimpleHeader getHeaderFromId(String headerId) {
		for (SimpleHeader h: headers) {
			if (h.getId().equals(headerId)) {
				return h;
			}
		}
		return null;
	}

	public boolean isHeaderMultiple(SimpleHeader header) {
		for (SimpleHeader h : headers){
			if (!h.getId().equals(header.getId())) { //avoid check with the same header
				if (header.hasSameName(h)){
					return true;
				}
			}
		}
		return false;
	}

	public JsonNode getSubjectHeaderAsJson() {
		JsonNodeFactory jsonFactory = JsonNodeFactory.instance;
		ObjectNode headerJson = jsonFactory.objectNode();
		headerJson.set(JsonConstants.SUBJECT_HEADER_ID, jsonFactory.textNode(subjHeader.getId()));
		headerJson.set(JsonConstants.SUBJECT_HEADER_PEARL_FEATURE, jsonFactory.textNode(getFeatureStructName(subjHeader)));
		headerJson.set(JsonConstants.SUBJECT_HEADER_NODE, subjHeader.getNodeConversion().toJson());
		headerJson.set(JsonConstants.SUBJECT_HEADER_GRAPH, subjHeader.getGraphApplication().toJson());
		ArrayNode additionalGraphsArray = jsonFactory.arrayNode();
		headerJson.set(JsonConstants.SUBJECT_HEADER_ADDITIONAL_GRAPHS, additionalGraphsArray);
		for (SimpleGraphApplication ga: subjHeader.getAdditionalGraphApplications()) {
			additionalGraphsArray.add(ga.toJson());
		}
		return headerJson;
	}

	public JsonNode getSimpleHeaderAsJson(SimpleHeader h) {
		JsonNodeFactory jsonFactory = JsonNodeFactory.instance;
		ObjectNode headerJson = jsonFactory.objectNode();

		headerJson.set(JsonConstants.SIMPLE_HEADER_ID, jsonFactory.textNode(h.getId()));
		headerJson.set(JsonConstants.SIMPLE_HEADER_NAME_STRUCT, h.getHeaderNameStruct().toJson());
		headerJson.set(JsonConstants.SIMPLE_HEADER_COL_INDEX, jsonFactory.numberNode(h.getColumnIdx()));
		headerJson.set(JsonConstants.SIMPLE_HEADER_PEARL_FEATURE, jsonFactory.textNode(getFeatureStructName(h)));
		headerJson.set(JsonConstants.SIMPLE_HEADER_IS_MULTIPLE, jsonFactory.booleanNode(isHeaderMultiple(h)));
		headerJson.set(JsonConstants.SIMPLE_HEADER_IGNORE, jsonFactory.booleanNode(h.isIgnore()));

		ArrayNode nodesArray = jsonFactory.arrayNode();
		headerJson.set(JsonConstants.SIMPLE_HEADER_NODES, nodesArray);
		for (NodeConversion n: h.getNodeConversions()) {
			nodesArray.add(n.toJson());
		}

		ArrayNode graphArray = jsonFactory.arrayNode();
		headerJson.set(JsonConstants.SIMPLE_HEADER_GRAPH, graphArray);
		for (GraphApplication ga: h.getGraphApplications()) {
			graphArray.add(ga.toJson());
		}

		return headerJson;
	}
	
}
