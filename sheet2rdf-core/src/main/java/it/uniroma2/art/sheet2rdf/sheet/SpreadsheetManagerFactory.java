package it.uniroma2.art.sheet2rdf.sheet;

import it.uniroma2.art.sheet2rdf.core.SpreadsheetOrDb;
import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.sheet.csv.CsvSpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.db.DBAllTablesManager;
import it.uniroma2.art.sheet2rdf.sheet.excel.XlsSpreadsheetManager;

import java.io.File;

public class SpreadsheetManagerFactory {

    /**
     * Instantiates the SpreadsheetManager implementation for the given file. Throws an exception if the file format
     * is not supported.
     * @param file
     * @return
     * @throws IllegalArgumentException
     */
    public static SpreadsheetManager getSpreadsheetManager(File file) throws IllegalArgumentException, GenericSheet2RDFException {
        String fileName = file.getName();
        String ext = fileName.substring(fileName.lastIndexOf("."));
        if (ext.equalsIgnoreCase(".xlsx") || ext.equalsIgnoreCase(".xls")){
            return new XlsSpreadsheetManager(file);
        } else if (ext.equalsIgnoreCase(".csv")){
            return new CsvSpreadsheetManager(file);
        } else
            throw new IllegalArgumentException("File format not supported. Supported formats: xls, xlsx, ods, csv");
    }

    public static SpreadsheetManager getSpreadsheetManager(SpreadsheetOrDb spreadsheetOrDb) throws IllegalArgumentException, GenericSheet2RDFException {

        if (spreadsheetOrDb.isForSpreadsheetFile()) {
            return getSpreadsheetManager(spreadsheetOrDb.getSpreadsheetFile());
        } else if(spreadsheetOrDb.isForDb()) {
            return new DBAllTablesManager(spreadsheetOrDb);
        } else {
            // this should never happen, but better safe than sorry
            throw new IllegalArgumentException("This should be called only by passing a spreadsheetOrDb to interact with a Table of a DB");
        }
    }

}
