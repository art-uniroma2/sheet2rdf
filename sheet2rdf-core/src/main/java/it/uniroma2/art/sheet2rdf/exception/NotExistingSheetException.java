package it.uniroma2.art.sheet2rdf.exception;

public class NotExistingSheetException extends Exception {

    public NotExistingSheetException(String msg) {
        super(msg);
    }

}
