package it.uniroma2.art.sheet2rdf.pearl;

import java.util.Map;

public interface PearlElement {

	/**
	 * Serializes a single pearl element
	 * @param tabs string containing tabulation to start from
	 * @return
	 */
	public String serialize(String tabs, Map<String, String> prefixMapping);
	
}
