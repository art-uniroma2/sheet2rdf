package it.uniroma2.art.sheet2rdf.cli;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.pf4j.PF4JComponentProvider;
import it.uniroma2.art.coda.structures.CODATriple;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import it.uniroma2.art.sheet2rdf.coda.Sheet2RDFCODA;
import it.uniroma2.art.sheet2rdf.core.Sheet2RDFCore;
import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.eclipse.rdf4j.common.iteration.Iterations;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.SKOSXL;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.RDFWriterRegistry;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

public class Sheet2RDF_CLI {

    private static final Logger logger = LoggerFactory.getLogger(Sheet2RDF_CLI.class);

    private static final String PROP_BUNDLE_FOLDER = "bundlesFolder";
    private static final String PROP_FELIX_CACHE = "felixCache";
    private static final String PROP_DATASHEET_FILE_PATH = "datasheetFile";
    private static final String PROP_INPUT_PEARL_FILE_PATH = "inputPearlFile";
    private static final String PROP_INPUT_FILE_PATH = "inputFile";
    private static final String PROP_OUTPUT_FILE_PATH = "outputFile";
    private static final String PROP_OUTPUT_FORMAT = "outputSerializationFormat";
    private static final String PROP_BASE_URI = "baseUri";
    private static final String PROP_MODEL_TYPE = "modelType";

    private static BufferedReader br;
    private static Properties prop;
    private static String propertiesFileName;
    private static String prop_inputPearlFile;
    private static String prop_baseUri;
    private static String prop_felixCachePath;
    private static String prop_bundleFolderPath;
    private static String prop_outputFilePath;
    private static RDFFormat outputRdfFormat;
    private static String prop_datasheetFilePath;

    public static void main(String[] args) throws Exception {

        Sheet2RDF_CLI s2rdfCli = new Sheet2RDF_CLI();

        br = new BufferedReader(new InputStreamReader(System.in));

        if (args.length < 1) {
            System.out.println("usage:\n" + Sheet2RDF_CLI.class.getName() + " <propertiesFilePath> ?<pearlFilePath>\n");
            return;
        }
        propertiesFileName = args[0];
        InputStream input = null;
        try {
            input = new FileInputStream(propertiesFileName);
        } catch (FileNotFoundException e) {
            System.out.println("'" + propertiesFileName + "' not found, or it is not a valid .properties file path");
            System.exit(1);
        }

        prop = new Properties();
        prop.load(input);

        //initialize and check properties
        s2rdfCli.initializeProperties();

        //initialize starting model
        Repository rep = new SailRepository(new MemoryStore());
        rep.init();
        RepositoryConnection connection = rep.getConnection();
        connection.setNamespace("", prop_baseUri + "/");

        SimpleValueFactory vf = SimpleValueFactory.getInstance();
        IRI rdfBaseURI = vf.createIRI("http://www.w3.org/1999/02/22-rdf-syntax-ns");
        IRI rdfsBaseURI = vf.createIRI("http://www.w3.org/2000/01/rdf-schema");
        IRI owlBaseURI = vf.createIRI("http://www.w3.org/2002/07/owl");
        IRI skosBaseURI = vf.createIRI("http://www.w3.org/2004/02/skos/core");
        IRI skosxlBaseURI = vf.createIRI("http://www.w3.org/2008/05/skos-xl");
        IRI ctxBaseURI = vf.createIRI(prop_baseUri);

        connection.add(Sheet2RDFCore.class.getResource("rdf.rdf"), rdfBaseURI.stringValue(),
                RDFFormat.RDFXML, rdfBaseURI);
        connection.add(Sheet2RDFCore.class.getResource("rdf-schema.rdf"), rdfsBaseURI.stringValue(),
                RDFFormat.RDFXML, rdfsBaseURI);
        connection.add(Sheet2RDFCore.class.getResource("owl.rdf"), owlBaseURI.stringValue(),
                RDFFormat.RDFXML, owlBaseURI);
        connection.add(Sheet2RDFCore.class.getResource("skos.rdf"), skosBaseURI.stringValue(),
                RDFFormat.RDFXML, skosBaseURI);
        connection.add(Sheet2RDFCore.class.getResource("skos-xl.rdf"), skosxlBaseURI.stringValue(),
                RDFFormat.RDFXML, skosxlBaseURI);
        connection.setNamespace("skosxl", SKOSXL.NAMESPACE); //this needs to be added manually (don't know why)

        File excelFile = new File(prop_datasheetFilePath);
        Sheet2RDFCore s2rdfCore = new Sheet2RDFCore(excelFile, connection);

        String firstSheetName = s2rdfCore.getSheetNames().get(0);

        //-----pearl-----
        File pearlFile;
        if (prop_inputPearlFile != null) { //Pearl provided
            pearlFile = new File(prop_inputPearlFile);
            logger.info("Pearl code:\n" + S2RDFUtils.pearlFileToString(pearlFile));
        } else { //pearl not provided: generating
            String pearlFilePath = "pearl_" + excelFile.getName().replace(".", "_") + ".pr";
            pearlFile = new File(pearlFilePath);
            System.out.println("Generating pearl file '" + pearlFile.getAbsolutePath() + "'...");
            s2rdfCore.generatePearlFile(pearlFile, firstSheetName);
            logger.info("Pearl file generated in '" + pearlFile.getAbsolutePath() + "'");
            logger.info("Pearl code:\n" + S2RDFUtils.pearlFileToString(pearlFile));
        }

        // if necessary, edit the pearl code
        try {
            while (!S2RDFUtils.checkPearl(pearlFile)) {
                waitForEnterPressed("Pearl code (in file " + pearlFile.getAbsolutePath() + ") contains some placeholders."
                        + " Please replace them, then press enter to continue...");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        System.out.println("Generating triples and write them on file...");
        //-----generate annotation-----
        JCas jcas = null;
        try {
            jcas = s2rdfCore.executeAnnotator(firstSheetName);
        } catch (UIMAException e) {
            e.printStackTrace();
            System.exit(1);
        }
        logger.info("UIMA Annotation generated");

        //-----generate suggested triples-----
        File bundlesFolder = new File(prop_bundleFolderPath);
        File felixCache = new File(prop_felixCachePath);
        felixCache.mkdirs();

        CODACore codaCore = null;
        try {
            PluginManager pluginManager = new DefaultPluginManager();
            pluginManager.loadPlugins();
            pluginManager.startPlugins();

            PF4JComponentProvider cp = new PF4JComponentProvider(pluginManager, true);
            codaCore = new CODACore(cp);
        } catch (Exception e1) {
            e1.printStackTrace();
            System.exit(1);
        }
        Sheet2RDFCODA s2rdfCoda = new Sheet2RDFCODA(connection, codaCore);

        try {
            List<SuggOntologyCoda> listSuggOntCoda = s2rdfCoda.suggestTriples(jcas, pearlFile);
            System.out.println("generated triples " + listSuggOntCoda.size());
            //adding triples to model
            for (SuggOntologyCoda sugg : listSuggOntCoda) {
                List<CODATriple> tripleList = sugg.getAllInsertARTTriple();
                for (CODATriple triple : tripleList) {
                    connection.add(triple.getSubject(), triple.getPredicate(), triple.getObject(), ctxBaseURI);
                    logger.info("Triple: (" + triple.getSubject().stringValue() + ", " + triple.getPredicate().stringValue()
                            + ", " + triple.getObject().stringValue() + ") added to model");
                }
            }
            //export model on rdf file
            RepositoryResult<Statement> stmts = connection.getStatements(null, null, null, false, ctxBaseURI);
            Model model = Iterations.addAll(stmts, new LinkedHashModel());
            try (FileOutputStream out = new FileOutputStream(prop_outputFilePath)) {
                RDFWriter writer = Rio.createWriter(outputRdfFormat, out);
                writer.startRDF();
                for (Statement st : model) {
                    writer.handleStatement(st);
                }
                writer.endRDF();
            }

            logger.info("Model written to " + prop_outputFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        System.out.println("Done");
        System.exit(0);
    }

    private static void waitForEnterPressed(String message) {
        System.out.print(message);
        try {
            br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void initializeProperties() {
        prop_baseUri = prop.getProperty(PROP_BASE_URI);
        if (prop_baseUri == null) {
            System.out.println("Property " + PROP_BASE_URI + " not specified. Please, check file " + propertiesFileName);
            System.exit(1);
        }
        prop_bundleFolderPath = prop.getProperty(PROP_BUNDLE_FOLDER, "bundles");
        prop_datasheetFilePath = prop.getProperty(PROP_DATASHEET_FILE_PATH);
        if (prop_datasheetFilePath == null) {
            System.out.println("Property " + PROP_DATASHEET_FILE_PATH + " not specified. Please, check file " + propertiesFileName);
            System.exit(1);
        }
        prop_inputPearlFile = prop.getProperty(PROP_INPUT_PEARL_FILE_PATH);
        prop_felixCachePath = prop.getProperty(PROP_FELIX_CACHE, "cache");
        String prop_modelType = prop.getProperty(PROP_MODEL_TYPE);
        if (prop_modelType == null || !prop_modelType.equals("SKOS") || !prop_modelType.equals("OWL")) {
            prop_modelType = "SKOS";
        }
        String prop_inputFilePath = prop.getProperty(PROP_INPUT_FILE_PATH);
        prop_outputFilePath = prop.getProperty(PROP_OUTPUT_FILE_PATH);
        if (prop_outputFilePath == null) {
            if (prop_inputFilePath == null) {
                prop_outputFilePath = "output.rdf";
            } else {
                prop_outputFilePath = prop_inputFilePath;
            }
        }

        String prop_outputRdfFormat = prop.getProperty(PROP_OUTPUT_FORMAT);
        if (prop_outputRdfFormat != null) {
            outputRdfFormat = RDFWriterRegistry.getInstance().getKeys().stream()
                    .filter(f -> f.getName().equals(prop_outputRdfFormat))
                    .findAny().orElse(null);
            if (outputRdfFormat == null) {
                System.out.println("Unknown RDFFormat " + prop_outputRdfFormat + " specified in property " + PROP_OUTPUT_FORMAT);
                System.exit(1);
            }
        } else { //try to infer the serialization format from the outuput file name
            String ext = FilenameUtils.getExtension(prop_outputFilePath);
            outputRdfFormat = RDFWriterRegistry.getInstance().getKeys().stream()
                    .filter(f -> f.getDefaultFileExtension().equals(ext))
                    .findAny().orElse(null);
            if (outputRdfFormat == null) {
                System.out.println("Cannot infer output serialization format from output file name " + prop_outputFilePath
                        + ". Please, specify a format in property " + PROP_OUTPUT_FORMAT
                        + " or change the extension of the output file name in property " + PROP_OUTPUT_FILE_PATH);
                System.exit(1);
            }
        }

        logger.info(PROP_BASE_URI + " = " + prop_baseUri);
        logger.info(PROP_BUNDLE_FOLDER + " = " + prop_bundleFolderPath);
        logger.info(PROP_DATASHEET_FILE_PATH + " = " + prop_datasheetFilePath);
        logger.info(PROP_INPUT_PEARL_FILE_PATH + " = " + prop_inputPearlFile);
        logger.info(PROP_FELIX_CACHE + " = " + prop_felixCachePath);
        logger.info(PROP_INPUT_FILE_PATH + " = " + prop_inputFilePath);
        logger.info(PROP_MODEL_TYPE + " = " + prop_modelType);
        logger.info(PROP_OUTPUT_FILE_PATH + " = " + prop_outputFilePath);
    }

}