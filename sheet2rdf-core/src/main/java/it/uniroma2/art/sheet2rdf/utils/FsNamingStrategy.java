package it.uniroma2.art.sheet2rdf.utils;

public enum FsNamingStrategy {

	columnAlphabeticIndex,
	columnNumericIndex,
	normalizedHeaderName 

}
