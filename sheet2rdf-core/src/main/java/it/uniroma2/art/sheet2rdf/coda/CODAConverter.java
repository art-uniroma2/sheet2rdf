package it.uniroma2.art.sheet2rdf.coda;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Joiner;
import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.coda.converters.contracts.DefaultConverter;
import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CODAConverter {
	
	private RDFCapabilityType type;
	private String contractUri;
	private String language; //in case type is "literal" this represents the language in "literal@it"
	private String datatypeUri; //in case type is "literal" this represents the datatype in "literal^^xsd:string"
	private String datatypeCapability; //datatype capability that the converter can produce (e.g. coda:date produces xsd:date)
	private Map<String, Object> params;
	/*
	 * A value of the params map can be:
	 * - String
	 * - List<String> (e.g args of TurtleCollection converter)
	 * - Map<String, String> (e.g. args parameter of random converter)
	 *
	 * where String (be it a List element, a value in a Map or the param value) can be a plain String, a node ref, an RDF Value serialized in NT
	 */
	
	public CODAConverter() {
		this(RDFCapabilityType.uri, DefaultConverter.CONTRACT_URI);
	}
	
	public CODAConverter(RDFCapabilityType type) {
		this(type, DefaultConverter.CONTRACT_URI);
	}
	
	public CODAConverter(RDFCapabilityType type, String contractUri) {
		this.type = type;
		this.contractUri = contractUri;
		this.params = new LinkedHashMap<>();
	}
	
	public RDFCapabilityType getType() {
		return type;
	}

	public void setType(RDFCapabilityType type) {
		this.type = type;
	}

	public String getContractUri() {
		return contractUri;
	}

	public void setContractUri(String contractUri) {
		this.contractUri = contractUri;
	}
	
	@JsonIgnore
	public String getContractQname() {
		return contractUri.replace(ContractConstants.CODA_CONTRACTS_BASE_URI, "coda:");
	}
	
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getDatatypeUri() {
		return datatypeUri;
	}
	
	public void setDatatypeUri(String datatypeUri) {
		this.datatypeUri = datatypeUri;
	}

	public String getDatatypeCapability() {
		return datatypeCapability;
	}

	public void setDatatypeCapability(String datatypeCapability) {
		this.datatypeCapability = datatypeCapability;
	}
	
	public Map<String, Object> getParams() {
		return params;
	}
	
	public void addParam(String name, Object value) {
		this.params.put(name, value);
	}
	
	public void setParams(Map<String,Object> params) {
		this.params = params;
	}
	
	public void removeParam(String name) {
		this.params.remove(name);
	}

	public String serialize(Map<String, String> prefixMapping) {
		String convString = type.name();
		
		if (type.equals(RDFCapabilityType.literal)) {
			if (language != null) {
				convString += "@" + language;
			} else if (datatypeUri != null) { //language and datatype are mutual exclusive
				String datatypeSerialization = "<" + datatypeUri + ">";
				for (Entry<String, String> mapEntry : prefixMapping.entrySet()) { //try to QName-ing the datatype
					String prefix = mapEntry.getKey();
					String ns = mapEntry.getValue();
					if (datatypeUri.startsWith(ns)) {
						datatypeSerialization = datatypeUri.replace(ns, prefix + ":");
						break;
					}
				}
				convString += "^^" + datatypeSerialization;
			}
		}
		
		if (!contractUri.equals(DefaultConverter.CONTRACT_URI)) {//if it's not the default converter
			convString += "("; //open round brackets of contract uri 
			convString += getContractQname();
			convString += "("; //open round brackets of converter parameters
			
			if (!params.isEmpty()) {
				Iterator<Entry<String, Object>> paramKeyValueIterator = params.entrySet().iterator();
				List<String> convParams = new ArrayList<>(); //list of parameters serialization to be joined later
				while (paramKeyValueIterator.hasNext()) {
					Entry<String, Object> paramKeyValue = paramKeyValueIterator.next();
					Object value = paramKeyValue.getValue();
					if (value instanceof String) {
						convParams.add(getStringValueNodeSerialization((String) value));
					} else if (value instanceof Map) {
						//value of a Map arg should always be a string (nodeRef/string/NT-serialization), I don't think we have nested Map in converter signature
						Map<String, String> valueAsMap = (Map<String, String>)value;
						Map<String, String> normalizedMap = new LinkedHashMap<>();
						for (Entry<String, String> e : valueAsMap.entrySet()) {
							String v = e.getValue();
							normalizedMap.put(e.getKey(), getStringValueNodeSerialization(v));
						}
						String mapSerialization = "{";
						mapSerialization += Joiner.on(",").withKeyValueSeparator(" = ").join(normalizedMap);
						mapSerialization += "}";
						convParams.add(mapSerialization);
					} else if (value instanceof List) {
						//value of a List arg should always be a string (nodeRef/string/NT-serialization), I don't think we have nested List in converter signature
						List<String> valueAsList = (List<String>)value;
						if (!valueAsList.isEmpty()) {
							List<String> valueAsStringList = new ArrayList<>();
							for (String v: valueAsList) {
								valueAsStringList.add(getStringValueNodeSerialization(v));
							}
							String listSerialization = String.join(",", valueAsStringList);
							convParams.add(listSerialization);
						}
					}
				}
				convString += String.join(",", convParams);
			}
			convString += ")"; //closes round brackets of converter parameters
			convString += ")"; //closes round brackets of contract uri
		}
		return convString;
	}

	/**
	 * Given a converter argument string, identify if it represent
	 * - a node reference (starts with $)
	 * - an RDF value (NT serialized)
	 * - a plain string
	 * and return the proper serialization (just wrap string in '', other cases returns as they are)
	 * @param s
	 * @return
	 */
	private String getStringValueNodeSerialization(String s) {
		if (s.startsWith("$")) {
			return s;
		} else {
			try {
				NTriplesUtil.parseValue(s, SimpleValueFactory.getInstance());
				return s;
			} catch (IllegalArgumentException e) {
				/*
				not a node referend ($foo) or a NT-serialized RDF value (<http://example#c1> or "foo"@en or "bar")
				=> wrap it in quotes, unless not already, and eventually escape '
				 */
				if (!(s.startsWith("'") && s.endsWith("'"))) {
					s = s.replace("'", "\\'");
					s = "'" + s + "'";
				}
				return s;
			}
		}
	}
	
	/**
	 * Check if the converter params contain a reference to another node id.
	 * This is useful in order to avoid the serialization in the pearl of a node definition that contains a reference
	 * to another node still not defined
	 * @return
	 */
	public boolean containsNodeReference() {
		if (!params.isEmpty()) {
			Iterator<Entry<String, Object>> it = params.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, Object> entry = it.next();
				Object value = entry.getValue();
				if (value instanceof Map) {
					Map<String, Object> valueAsMap = (Map<String, Object>)value;
					for (Entry<String, Object> e : valueAsMap.entrySet()) {
						Object v = e.getValue();
						if (v instanceof String) { //reference to a nodeId
							return true;
						}
					}
				} else if (value instanceof List) {
					List<Object> valueAsList = (List<Object>)value;
					for (Object v: valueAsList) {
						if (v instanceof String) { //reference to a nodeId
							return true; 
						}
					}
				}
			}
		}
		return false;
	}
	
	public String toString() {
		String s = "";
		s += "type:\t\t" + this.type + "\n";
		s += "contract:\t" + this.contractUri + "\n";
		s += "lang:\t\t" + this.language + "\n";
		s += "dt:\t\t" + this.datatypeUri + "\n";
		s += "params:\t\t" + this.params;
		return s;
	}

}
