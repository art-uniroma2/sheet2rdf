package it.uniroma2.art.sheet2rdf.core;

import java.util.List;

import org.apache.uima.UIMAException;
import org.apache.uima.cas.CAS;
import org.apache.uima.fit.factory.TypeSystemDescriptionFactory;
import org.apache.uima.resource.metadata.TypeDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;

import it.uniroma2.art.sheet2rdf.header.SimpleHeader;

/** 
 * This class allows to generate dynamically a TypeSystemDescription based on an Excel sheet.
 * The TSD will be composed by a static feature representing the subject and one more features for every
 * headers in the excel file. The value(s) of those features are FeatureStructure containing row and column
 * indexes and the value of the excel cell.
 * 
 * @author Tiziano Lorenzetti
 *
 */
public class TypeSystemDescriptionCreator {
	
	private static final String VALUE_FEATURE_NAME = "value";
	private static final String ROW_FEATURE_NAME = "row";
	private static final String COLUMN_FEATURE_NAME = "column";
	
	/**
	 * Creates and returns a TypeSystemDescription based on the spreadsheet headers. The TSD will be composed
	 * by a static feature representing the subject and one more features for every headers in the excel file.
	 * The value(s) of those features are FeatureStructure containing row and column indexes and the value of
	 * the excel cell.
	 * @param headersStruct Structure containing Header of the spreadseeth
	 * @param annotationTypeName name of the type to be created and customized with features
	 * @param cellFSName name of the inner FeatureStructure representing a cell FS
	 * @return
	 * @throws UIMAException
	 */
	public static TypeSystemDescription createTypeSystemDescription(MappingStruct mappingStruct, String annotationTypeName, String cellFSName) throws UIMAException{

		TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription();

		// Type System Description creation. Adding ExcelAnnotation.
		TypeDescription sheet2rdfAnnotationType = tsd.addType(annotationTypeName, "", CAS.TYPE_NAME_ANNOTATION);
		//FeatureStructure for value of ExcelAnnotation's feature
		TypeDescription cellFSType = tsd.addType(cellFSName, "", CAS.TYPE_NAME_TOP);
		cellFSType.addFeature(VALUE_FEATURE_NAME, "", CAS.TYPE_NAME_STRING);
		cellFSType.addFeature(ROW_FEATURE_NAME, "", CAS.TYPE_NAME_INTEGER);
		cellFSType.addFeature(COLUMN_FEATURE_NAME, "", CAS.TYPE_NAME_INTEGER);
		
		// Adding feature to ExcelAnnotation, based on the excel sheet headers
		List<SimpleHeader> headers = mappingStruct.getHeaders();
		for (int i=0; i<headers.size(); i++){
			SimpleHeader header = headers.get(i);
			sheet2rdfAnnotationType.addFeature(mappingStruct.getFeatureStructureName(header.getId()), "", cellFSType.getName());
//			System.out.println("Added feature " + header.getFeatureStructureName() + " (" + cellFSType.getName() + ") to type " + sheet2rdfAnnotationType.getName());
		}
		return tsd;
	}

}
