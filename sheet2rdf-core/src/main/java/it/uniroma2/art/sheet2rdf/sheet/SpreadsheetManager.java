package it.uniroma2.art.sheet2rdf.sheet;

import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;

import java.util.List;
import java.util.Map;

public interface SpreadsheetManager {

    String PREFIX_MAPPING_SHEET_NAME = "prefix_mapping";

    /**
     * Returns the prefix namespace mapping (optionally) defined in a dedicated sheet
     * @return
     */
    Map<String, String> getPrefixNamespaceMapping();

    /**
     * Returns the list of tables
     * @return
     */
    List<S2RDFSheet> getSheets();

    /**
     * Returns the sheet manager at the given index
     * @param index
     * @return
     */
    S2RDFSheet getSheet(int index) throws NotExistingSheetException;

    /**
     * Returns the sheet manager of the sheet with the given name
     * @param sheetName
     * @return
     */
    S2RDFSheet getSheet(String sheetName) throws NotExistingSheetException;

    /**
     * Returns the amount of data sheets (prefix mapping excluded)
     * @return
     */
    int getDataSheetsCount();



}
