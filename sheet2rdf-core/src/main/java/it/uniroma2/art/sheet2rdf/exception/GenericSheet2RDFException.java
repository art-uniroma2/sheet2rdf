package it.uniroma2.art.sheet2rdf.exception;

public class GenericSheet2RDFException extends Exception {
    public GenericSheet2RDFException(Exception e) {
        super(e);
    }
}
