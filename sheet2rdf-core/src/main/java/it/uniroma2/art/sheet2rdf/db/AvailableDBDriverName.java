package it.uniroma2.art.sheet2rdf.db;

public class AvailableDBDriverName {
    public enum Values {MySQL, MariaDB, PostgreSQL }

    public static String getClassNameFromDriver(Values driverName){
        switch (driverName) {
            case MySQL:
                return "com.mysql.cj.jdbc.Driver";
            case MariaDB:
                return "org.mariadb.jdbc.Driver";
            case PostgreSQL:
                return "org.postgresql.Driver";
        }
        // this cannot happen, since the only possible values for driverName are all managed
        return null;
    }
}
