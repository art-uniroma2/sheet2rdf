package it.uniroma2.art.sheet2rdf.resolver;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.SKOS;

import it.uniroma2.art.coda.interfaces.annotations.converters.RDFCapabilityType;
import it.uniroma2.art.sheet2rdf.coda.CODAConverter;

public class ConverterResolver {
	
	/**
	 * Given a class returns the converter to use
	 * @param cls
	 * @return
	 */
	public static CODAConverter getConverter(IRI cls) {
		if (cls.equals(RDFS.CLASS) || cls.equals(RDFS.RESOURCE) || cls.equals(OWL.CLASS) || cls.equals(OWL.THING) ||
				cls.equals(SKOS.CONCEPT_SCHEME)) {
			return new CODAConverter(RDFCapabilityType.uri);
		} else if (cls.equals(RDFS.LITERAL)) {
			return new CODAConverter(RDFCapabilityType.literal);
		} else if (cls.equals(SKOS.CONCEPT)) {
			return new CODAConverter(RDFCapabilityType.uri);
//		} else if (cls.equals(SKOSXL.LABEL)) {
//			return new CODAConverter(RDFCapabilityType.uri, RandomIdGenerator.CONTRACT_URI, "xLabel");
		} else {
			return null;
		}
	}
	
}
