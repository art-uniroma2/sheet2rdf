package it.uniroma2.art.sheet2rdf.core;

import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;
import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.CasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Annotator UIMA. This class creates annotations structured with the TypeSystemDescription created with
 * TypeSystemDescriptionCreator
 * @author Tiziano Lorenzetti
 *
 */
public class SheetAnnotator extends CasAnnotator_ImplBase{

	private static final String VALUE_FEATURE_NAME = "value";
	private static final String ROW_FEATURE_NAME = "row";
	private static final String COLUMN_FEATURE_NAME = "column";
	
	public static final String EXCEL_FILE = "excelFile";
	@ConfigurationParameter(name = EXCEL_FILE)
	private String excelFile;

	public static final String SHEET_NAME = "sheetName";
	@ConfigurationParameter(name = SHEET_NAME)
	private String sheetName;
	
	public static final String ANNOTATION_TYPE_STRING = "annotationType";
	@ConfigurationParameter(name = ANNOTATION_TYPE_STRING)
	private String annotationType;
	
	public static final String CELL_FS_NAME_STRING = "cellFSName";
	@ConfigurationParameter(name = CELL_FS_NAME_STRING)
	private String cellFSName;
	
	/*
	 * UIMA Fit doesn't provide an easy way to pass a Map as parameter to an annotator.
	 * As workaround, I pass the Map <normalized_headerName, fs_id> as a List (accepted as parameter) where the
	 * even elements are the keys and the odd elements are the values. The List will be converted to map during
	 * the execution of the annotator.
	 */
	public static final String HEADER_FS_MAP = "headerFsMap_asList";
	@ConfigurationParameter(name = HEADER_FS_MAP)
	private List<String> headerFsMap_asList;

	@Override
	public void process(CAS aCAS) throws AnalysisEngineProcessException {
		
		TypeSystem ts = aCAS.getTypeSystem();
		Type type = ts.getType(annotationType);
		Type cellFSType = ts.getType(cellFSName);

		S2RDFSheet sheet;
		try {
			sheet = SpreadsheetManagerFactory.getSpreadsheetManager(new File(excelFile)).getSheet(sheetName);
		} catch (NotExistingSheetException | GenericSheet2RDFException e) {
			throw new AnalysisEngineProcessException(e);
		}

		//convert header-id mapping list into map
		Map<String, String> headerFsMap = new HashMap<>();
		for (int i = 0; i < headerFsMap_asList.size(); i += 2) {
			headerFsMap.put(headerFsMap_asList.get(i), headerFsMap_asList.get(i+1));
		}

		List<String> headersList = sheet.getHeaders();
		int nColumn = sheet.getColumnCount();
		int nRow = sheet.getRowCount();

		//for every row create an annotation and populate with the data fetched from the cells
		for (int i = 0; i < nRow; i++){
			//create annotation ExcelAnnotation
			AnnotationFS ann = aCAS.createAnnotation(type, i, i);//i as begin and end (last 2 params)
			
			FeatureStructure valueFS;
			Feature valueFeat = cellFSType.getFeatureByBaseName(VALUE_FEATURE_NAME);
			Feature rowFeat = cellFSType.getFeatureByBaseName(ROW_FEATURE_NAME);
			Feature columnFeat = cellFSType.getFeatureByBaseName(COLUMN_FEATURE_NAME);
			
			//populating features based on headers
			for (int j = 0; j < nColumn; j++){
				String headerName = headersList.get(j);
				Feature feat = type.getFeatureByBaseName(headerFsMap.get(S2RDFUtils.getHeaderId(headerName, j)));
				String value = sheet.getCellValue(i, j);
				if (!value.equals("")){
					valueFS = aCAS.createFS(cellFSType);
					valueFS.setStringValue(valueFeat, value);
					valueFS.setIntValue(rowFeat, i);
					valueFS.setIntValue(columnFeat, j);
					ann.setFeatureValue(feat, valueFS);
//					System.out.println("Added "+value+" to feature "+feat+" row "+ valueFS.getIntValue(rowFeat) +" col "+valueFS.getIntValue(columnFeat));
				}
			}
			aCAS.addFsToIndexes(ann);
		}
	}

}
