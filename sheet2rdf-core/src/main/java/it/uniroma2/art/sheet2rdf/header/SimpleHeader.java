package it.uniroma2.art.sheet2rdf.header;

import java.util.ArrayList;
import java.util.List;

import it.uniroma2.art.sheet2rdf.utils.S2RDFUtils;

public class SimpleHeader extends Header {
	
	private int columnIdx; //useful for naming of feature struct
	private HeaderNameStruct headerNameStruct;
	
	private List<NodeConversion> nodeConversions;
	private List<GraphApplication> graphApplications;
	
	private boolean ignore = false; //true if the column should be ignored during the conversion process

	public SimpleHeader(HeaderNameStruct headerNameStruct, int columnIdx) {
		this.headerNameStruct = headerNameStruct;
		this.id = S2RDFUtils.getHeaderId(headerNameStruct.getFullName(), columnIdx);
		this.columnIdx = columnIdx;
		this.nodeConversions = new ArrayList<>();
		this.graphApplications = new ArrayList<>();
	}

	public int getColumnIdx() {
		return columnIdx;
	}

	public void setColumnIdx(int columnIdx) {
		this.columnIdx = columnIdx;
	}
	
	public HeaderNameStruct getHeaderNameStruct() {
		return headerNameStruct;
	}

	public void setHeaderName(HeaderNameStruct headerNameStruct) {
		this.headerNameStruct = headerNameStruct;
	}

	public List<NodeConversion> getNodeConversions() {
		return nodeConversions;
	}

	public NodeConversion getNodeConversion(String nodeId) {
		return this.nodeConversions.stream().filter(n -> n.getNodeId().equals(nodeId)).findAny().orElse(null);
	}

	public void setNodeConversions(List<NodeConversion> conversions) {
		this.nodeConversions = conversions;
	}
	
	public void addNodeConversions(NodeConversion conversion) {
		this.nodeConversions.add(conversion);
	}
	
	public List<GraphApplication> getGraphApplications() {
		return graphApplications;
	}

	public void setGraphApplications(List<GraphApplication> graphApplications) {
		this.graphApplications = graphApplications;
	}
	
	public void addGraphApplication(GraphApplication graphApplication) {
		this.graphApplications.add(graphApplication);
	}

	public boolean isIgnore() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}
	
	
	public boolean hasSameName(SimpleHeader header) {
		return this.headerNameStruct.getName().equals(header.getHeaderNameStruct().getName());
	}
	
	public String toString() {
		String s = "";
		s += "id:\t" + this.id + "\n";
		s += "col:\t" + this.columnIdx + "\n";
		s += "name:\t" + this.headerNameStruct + "\n";
		s += "nodes:\n" + this.nodeConversions + "\n";
		s += "graphs:\n" + this.graphApplications + "\n";
		s += "ignore:\n" + this.ignore + "\n";
		return s;
	}
	
}
