package it.uniroma2.art.sheet2rdf.core;

import it.uniroma2.art.sheet2rdf.db.AvailableDBDriverName;
import it.uniroma2.art.sheet2rdf.db.DBInfo;

import java.io.File;
import java.util.List;

public class SpreadsheetOrDb {

    private File spreadsheetFile = null; // used only when dealing with a spreadsheet, otherwise is null

    DBInfo dbInfo = null; // used only when dealing with a DB, otherwise it is null

    public SpreadsheetOrDb(File spreadsheetFile) {
        this.spreadsheetFile = spreadsheetFile;
    }

    public SpreadsheetOrDb(String db_base_url, String db_name, List<String> db_tableList, String db_user, String db_password,
                           AvailableDBDriverName.Values db_driverName) {

        dbInfo = new DBInfo(db_base_url, db_name, db_tableList, db_user, db_password, db_driverName);
    }

    public SpreadsheetOrDb(DBInfo dbInfo) {
        this.dbInfo = dbInfo;
    }

    public File getSpreadsheetFile() {
        return spreadsheetFile;
    }

    public boolean isForSpreadsheetFile() {
        return spreadsheetFile != null;
    }

    public boolean isForDb() {
        return dbInfo != null;
    }

    public DBInfo getDbInfo() {
        return dbInfo;
    }

}

