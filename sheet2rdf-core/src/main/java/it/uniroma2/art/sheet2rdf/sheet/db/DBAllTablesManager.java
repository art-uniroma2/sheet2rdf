package it.uniroma2.art.sheet2rdf.sheet.db;

import it.uniroma2.art.sheet2rdf.core.SpreadsheetOrDb;
import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.sheet.AbstractSpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class DBAllTablesManager extends AbstractSpreadsheetManager {

    public DBAllTablesManager(SpreadsheetOrDb spreadsheetOrDb) throws GenericSheet2RDFException {
        sheetsMap = new LinkedHashMap<>();
        prefixMappings = new HashMap<>();
        //iterate over all tables in spreadsheetOrDb to add them in dataSheetManagers
        for (String tableName : spreadsheetOrDb.getDbInfo().getDb_tableList()) {
            DBTableManager dbTableManager = new DBTableManager(spreadsheetOrDb, tableName);
            S2RDFSheet sheet = new S2RDFSheet(dbTableManager);
            sheetsMap.put(tableName, sheet);
        }
    }
}
