package it.uniroma2.art.sheet2rdf.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.helpers.NTriplesUtil;

public class S2RDFUtils {

	/**
	 * Returns the content of the given pearl file (.pr)
	 * @param pearlFile
	 * @return
	 * @throws IOException
	 */
	public static String pearlFileToString(File pearlFile) throws IOException {
		if (!pearlFile.getName().endsWith(".pr"))
			throw new IOException("input file is not a pear file (.pr)");
		String pearlCode = "";
		BufferedReader input = new BufferedReader(new FileReader(pearlFile));
		StringBuffer buffer = new StringBuffer();
		while ((pearlCode = input.readLine()) != null)
			buffer.append(pearlCode + "\n");
		input.close();
		pearlCode = buffer.toString();
		return pearlCode;
	}
	
	/**
	 * Checks if a pearl file is ready to be given in input to CODA. It returns true if the pearl file is
	 * correct, false otherwise.
	 * @param pearlFile
	 * @return
	 * @throws IOException
	 */
	public static boolean checkPearl(File pearlFile) throws IOException{
		String pearlCode = pearlFileToString(pearlFile);
		return !pearlCode.contains("%pls_provide_");
	}
	
	/**
	 * Returns true if the given resource is a class
	 * @param resource
	 * @param connection
	 * @return
	 */
	public static boolean isClass(Resource resource, RepositoryConnection connection) {
		String query = "ASK { \n"
				+ "VALUES(?cls) { \n"
				+ "(" + NTriplesUtil.toNTriplesString(RDFS.CLASS) + ")"
				+ "(" + NTriplesUtil.toNTriplesString(OWL.CLASS) + ") \n"
				+ "} \n"
				+ NTriplesUtil.toNTriplesString(resource) + " a ?cls \n"
				+ "}";
		return connection.prepareBooleanQuery(query).evaluate();
	}
	
	/**
	 * 
	 * @param resource
	 * @param connection
	 * @return
	 */
	public static boolean isProperty(Resource resource, RepositoryConnection connection) {
		String query = "ASK { \n"
				+ "VALUES(?propCls) { \n"
				+ "(" + NTriplesUtil.toNTriplesString(RDF.PROPERTY) + ")"
				+ "(" + NTriplesUtil.toNTriplesString(OWL.ANNOTATIONPROPERTY) + ")"
				+ "(" + NTriplesUtil.toNTriplesString(OWL.DATATYPEPROPERTY) + ")"
				+ "(" + NTriplesUtil.toNTriplesString(OWL.ONTOLOGYPROPERTY) + ")"
				+ "(" + NTriplesUtil.toNTriplesString(OWL.OBJECTPROPERTY) + ") \n"
				+ "} \n"
				+ NTriplesUtil.toNTriplesString(resource) + " a ?propCls \n"
				+ "}";
		return connection.prepareBooleanQuery(query).evaluate();
	}
	
	/**
	 * Checks if an header represents a qname.
	 * @param header
	 * @return
	 */
	public static boolean isQName(String header, Map<String, String> prefixMapping){
		header = header.trim();
		int end = header.indexOf(":");
		if (end >= 0){
			String prefix = header.substring(0, end);
			return prefixMapping.containsKey(prefix);
		} else
			return false;
	}
	
	/**
	 * Expand a qname into an uri using the provided prefix mappings map.
	 * Returns the expanded URI if a prefix is available, otherwise returns the qname as it is.
	 * 
	 * @param qname
	 * @param prefixMapping
	 * @return
	 */
	public static String expandQName(String qname, Map<String, String> prefixMapping){
		String s = qname;
		int idx = qname.indexOf(":");
		if (idx >= 0){
			String pref = qname.substring(0, idx);
			if (prefixMapping.containsKey(pref)){
				String ns = prefixMapping.get(pref);
				s = qname.replace(pref+":", ns);
			}
		}
		return s;
	}
	
	/**
	 * Collapses a IRI as Qname using the provided prefix mapping map.
	 * Returns the qname if a the IRI contains a recognized namespace, otherwise returns null.
	 * @param iri
	 * @param prefixMapping
	 * @return
	 */
	public static String asQName(IRI iri, Map<String, String> prefixMapping) {
		String qname = null;
		for (String pref: prefixMapping.keySet()) {
			String ns = prefixMapping.get(pref);
			if (iri.getNamespace().equals(ns)) {
				String iriString = iri.stringValue();
				qname = iriString.replace(ns, pref + ":");
				break;
			}
		}
		return qname;
	}
	
	
	/**
	 * UIMA feature accept only names with letters, digits and underscore.
	 * This method replace some character and returns the name to assign to the feature name compliant with
	 * UIMA constraints. 
	 * 
	 * @param s name of the header
	 * @param columnIdx column index of the header (useful to assign a different FSName to 
	 * different columns with same header
	 * @return
	 */
	public static String getFeatureNameFromHeader(String s, int columnIdx){
		s = s.trim();
		s = s.replaceAll("[\\(\\):]", "_"); //replace (): with _
		//remove everything that is not letters (upper/lower case) digits or underscore
		s = s.replaceAll("[^a-zA-Z0-9_]", "");
		//replace multiple _ with single _
		s = s.replaceAll("_+", "_");
		//UIMA feature cannot starts with _ char, if it's so, remove it
		while (s.startsWith("_")){
			s = s.substring(1);
		}
		//remove also ending _ in order to make the feature name cleaner
		while (s.endsWith("_")){
			s = s.substring(0, s.length()-1);
		}
		return "col" + columnIdx + "_" + s;
	}
	
	public static String getHeaderId(String headerName, int columnIdx) {
		//replace with _ everything that is not letters (upper/lower case) or digits
		headerName = headerName.replaceAll("[^a-zA-Z0-9]", "_");
		return "col" + columnIdx + "_" + headerName;
	}
	
	/**
	 * Replaces the placeholders (e.g. $node_id) in the graphPattern according the given replacementMap
	 * @param graphPattern
	 * @param replacementMap
	 * @return
	 */
	//TODO use this also in MappingStruct
	public static String replaceNodesIdInGraphPattern(String graphPattern, Map<String, String> replacementMap) {
		String targetPattern = graphPattern;
		for (Entry<String, String> replacement: replacementMap.entrySet()) {
			//leading $ and trailing space, is to be sure that the replace affects only the placeholders
			targetPattern = targetPattern.replace("$" + replacement.getKey() + " ", "$" + replacement.getValue() + " ");
		}
		return targetPattern;
	}
	
	/**
	 * Returns a copy of the input converter parameters map, with the values replaced according the given
	 * replacementMap. A value is replaced only if it is a reference to a node Id.
	 * @param params
	 * @param replacementMap
	 * @return
	 */
	public static Map<String, Object> replaceNodesIdInConverterParams(Map<String, Object> params, Map<String, String> replacementMap) {
		Map<String, Object> replacedParams = new LinkedHashMap<>();
		for (Entry<String, Object> param: params.entrySet()) {
			String paramKey = param.getKey();
			Object paramValue = param.getValue();
			if (paramValue instanceof String) { //string => copy as it is
				replacedParams.put(paramKey, paramValue);
			} else if (paramValue instanceof List) { //list of rdf4j Value or nodeId (string).
				List<Object> valueAsList = (List<Object>)paramValue;
				List<Object> replacedListValue = new ArrayList<>();
				for (Object v: valueAsList) {
					if (v instanceof Value) {//value represents a rdf4j Values => copy as it is
						replacedListValue.add(paramValue);
					} else if (v instanceof String ){ //string, value represents a nodeId => copy replacing the node id
						replacedListValue.add(replacementMap.get(v));
					}
				}
				replacedParams.put(paramKey, replacedListValue);
			} else if (paramValue instanceof Map) { //Map with value that could be a rdf4j Value or a nodeId (string).
				Map<String, Object> valueAsMap = (Map<String, Object>)paramValue;
				Map<String, Object> replacedMapValue = new LinkedHashMap<>();
				for (Entry<String, Object> e : valueAsMap.entrySet()) {
					Object v = e.getValue();
					if (v instanceof Value) {//value represents a rdf4j Values => copy as it is
						replacedMapValue.put(e.getKey(), v); 
					} else if (v instanceof String) { //string, value represents a nodeId => copy replacing the node id
						//substring cause in a map argument, the ID of a node starts with $, while replacementMap contains the id without $
						String newValue = replacementMap.get(((String) v).substring(1));
						replacedMapValue.put(e.getKey(), "$" + newValue);
					}
				}
				replacedParams.put(paramKey, replacedMapValue);
			}
		}
		return replacedParams;
	}
	
}
