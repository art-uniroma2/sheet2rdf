package it.uniroma2.art.sheet2rdf.sheet.excel;

import it.uniroma2.art.coda.converters.commons.DateTimeUtils;
import it.uniroma2.art.sheet2rdf.sheet.SheetManager;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.NumberToTextConverter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class XlsSheetManager implements SheetManager {

    private final Sheet sheet;

    public XlsSheetManager(Sheet sheet) {
        this.sheet = sheet;
    }

    @Override
    public String getSheetName() {
        return this.sheet.getSheetName();
    }

    @Override
    public List<String> getHeaders(boolean includeDuplicate) {
        List<String> headers = new ArrayList<>();
        Row headerRow = sheet.getRow(0);
        int nCol = getColumnCount();
        for (int i = 0; i < nCol; i++) {
            Cell headerCell = headerRow.getCell(i);
            String header = headerCell.getStringCellValue().trim();
            if (!headers.contains(header))//if the headers is not yet in the list, add it
                headers.add(header);
            else { //else add it only if includeDuplicate = true
                if (includeDuplicate)
                    headers.add(header);
            }
        }
        return headers;
    }

    @Override
    public List<List<String>> getDataTable() {
        int rows = getRowCount();
        int columns = getColumnCount();
        List<List<String>> table = new ArrayList<>();
        for (int r = 1; r < rows; r++) {
            List<String> arrayListRow = new ArrayList<>();
            for (int c = 0; c < columns; c++) {
                String data = getCellValue(r, c);
                arrayListRow.add(data);
            }
            table.add(arrayListRow);
        }
        return table;
    }

    @Override
    public String getCellValue(int row, int column) {
        String value = "";
        Row r = sheet.getRow(row);
        if (r != null) {
            Cell cell = r.getCell(column);
            if (cell != null) {
                if (cell.getCellType() == CellType.NUMERIC) {
                    if (DateUtil.isCellDateFormatted(cell)) { //data
                        SimpleDateFormat sdf = new SimpleDateFormat(DateTimeUtils.DATE_PATTERN_ISO_8601);
                        value = sdf.format(cell.getDateCellValue());
                    } else { //number
                        value = NumberToTextConverter.toText(cell.getNumericCellValue());
                    }
                } else if (cell.getCellType() == CellType.STRING) { //String
                    value = cell.getStringCellValue();
                } else if (cell.getCellType() == CellType.FORMULA) {
                    switch (cell.getCachedFormulaResultType()) {
                        case BOOLEAN:
                            value = cell.getBooleanCellValue()+"";
                            break;
                        case NUMERIC:
                            value = NumberToTextConverter.toText(cell.getNumericCellValue());
                            break;
                        case STRING:
                            value = cell.getRichStringCellValue().getString();
                            break;
                    }
                }
            }
        }
        return value;
    }

    @Override
    public boolean isMultipleHeader(String headerValue) {
        Row headerRow = sheet.getRow(0);
        int nCol = getColumnCount();
        int idxHeaderValue = 0;
        //find the column index of the headerValue
        for (int i = 0; i < nCol; i++) {
            Cell headerCell = headerRow.getCell(i);
            String h = headerCell.getStringCellValue().trim();
            if (h.equals(headerValue)) {
                idxHeaderValue = i;
                break;
            }
        }
        //check if following column has the same headerValue
        Cell followingHeader = headerRow.getCell(idxHeaderValue + 1);
        if (followingHeader != null) {//check necessary cause the header could be the last one
            return followingHeader.getStringCellValue().trim().equals(headerValue);
        } else {
            return false;
        }
    }

    @Override
    public int getColumnCount() {
        int col;
        int lastCellNum = sheet.getRow(0).getLastCellNum();
        //if a cell has been removed simply selecting and then deleting it,
        //getLastCellNum still considers the empty cell and alters the count
        //so, starting from the last cell, check backward the last not-empty cell
        for (col = lastCellNum - 1; col > 0; col--) {
            Cell lastCell = sheet.getRow(0).getCell(col);
            if (lastCell.getCellType() == CellType.STRING && !lastCell.getStringCellValue().trim().isEmpty()) {
                break;
            }
        }
        return col + 1; //+1 cause the index of getCell is 0-based
    }

    @Override
    public int getRowCount() {
        int rowCount;
        int phisicalRow = sheet.getPhysicalNumberOfRows();
        if (phisicalRow == 0) return 0;
        //if every cell of a row has been removed simply selecting and then deleting it,
        //getPhysicalNumberOfRows still considers the empty cell and alters the count
        //so, starting from the last row, check backward the last not-empty row
        int col = getColumnCount();
        emptyRowSearch:
        for (rowCount = phisicalRow - 1; rowCount >= 0; rowCount--) {
            Row row = sheet.getRow(rowCount);
            if (row == null) continue; //prevents NullPointerException for wrong getPhysicalNumberOfRows result
            //check if the row is empty
            for (int i = 0; i < col; i++) {
                Cell cell = row.getCell(i);
                if (cell != null && cell.getCellType() != CellType.BLANK) {
                    break emptyRowSearch;//the row is not empty, so stop checking backward
                }
            }
        }
        return rowCount + 1; //+1 cause the index of getRow is 0-based
    }

}
