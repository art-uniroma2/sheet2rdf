package it.uniroma2.art.sheet2rdf.core;

import it.uniroma2.art.coda.converters.contracts.ContractConstants;
import it.uniroma2.art.sheet2rdf.db.AvailableDBDriverName;
import it.uniroma2.art.sheet2rdf.exception.GenericSheet2RDFException;
import it.uniroma2.art.sheet2rdf.exception.NotExistingSheetException;
import it.uniroma2.art.sheet2rdf.sheet.S2RDFSheet;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManager;
import it.uniroma2.art.sheet2rdf.sheet.SpreadsheetManagerFactory;
import it.uniroma2.art.sheet2rdf.utils.FsNamingStrategy;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.vocabulary.XSD;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Sheet2RDFCore {
	
	private final SpreadsheetOrDb spreadsheetOrDb;

	private Map<String, MappingStruct> mappingStructMap; //map: table/sheet name -> MappingStruct
	private SpreadsheetManager spreadsheetManager;
	private Map<String, String> mergedPrefixMapping; //prefix mapping of s2rdf + project + spreadsheet

	private static final String annotationTypeName = "it.uniroma2.art.Sheet2RDFAnnotation";
	private static final String cellFSName = "it.uniroma2.art.CellFeatureStructure"; 
	
	public Sheet2RDFCore(File spreadsheetFile, RepositoryConnection connection) throws GenericSheet2RDFException {
		this(spreadsheetFile, connection, FsNamingStrategy.columnNumericIndex);
	}
	
	/**
	 * @param spreadsheetFile
	 * @param connection
	 * @param fsStrategy
	 */
	public Sheet2RDFCore(File spreadsheetFile, RepositoryConnection connection, FsNamingStrategy fsStrategy) throws GenericSheet2RDFException {
		spreadsheetOrDb = new SpreadsheetOrDb(spreadsheetFile);
		this.initSheet2RDFCore(spreadsheetOrDb, connection, fsStrategy);
	}

	public Sheet2RDFCore(String db_base_url, String db_name, List<String> db_tableList, String db_user, String db_password,
						 AvailableDBDriverName.Values db_driverName, RepositoryConnection connection) throws GenericSheet2RDFException {
		this(db_base_url, db_name, db_tableList, db_user, db_password, db_driverName, connection,
				FsNamingStrategy.columnNumericIndex);
	}

	public Sheet2RDFCore(String db_base_url, String db_name, List<String> db_tableList, String db_user, String db_password,
						 AvailableDBDriverName.Values db_driverName, RepositoryConnection connection,
						 FsNamingStrategy fsStrategy) throws GenericSheet2RDFException {
		spreadsheetOrDb = new SpreadsheetOrDb(db_base_url, db_name, db_tableList, db_user, db_password,
				db_driverName);
		this.initSheet2RDFCore(spreadsheetOrDb, connection, fsStrategy);
	}

	private void initSheet2RDFCore(SpreadsheetOrDb spreadsheetOrDb, RepositoryConnection connection, FsNamingStrategy fsStrategy) throws GenericSheet2RDFException {
		//init the headers structure with the sheet headers
		spreadsheetManager = SpreadsheetManagerFactory.getSpreadsheetManager(spreadsheetOrDb);

		mappingStructMap = new LinkedHashMap<>();
		initMergedPrefixMappings(connection);

		List<S2RDFSheet> sheets = spreadsheetManager.getSheets();
		for (S2RDFSheet s : sheets) {
			List<String> headerList = s.getHeaders();
			//init the mapping structure for the current sheet with the sheet headers
			MappingStruct mappingStruct = new MappingStruct(headerList, connection, mergedPrefixMapping, fsStrategy);
			mappingStructMap.put(s.getName(), mappingStruct);
		}
	}

	public SpreadsheetManager getSpreadsheetManager() {
		return this.spreadsheetManager;
	}
	
	public MappingStruct getMappingStruct(String sheetName) {
		return this.mappingStructMap.get(sheetName);
	}

	public Map<String, MappingStruct> getMappingStructMap() {
		return this.mappingStructMap;
	}

	public List<String>getSheetNames() {
		return new ArrayList<>(this.mappingStructMap.keySet());
	}
	
	/**
	 * Merges prefix mapping of project, of s2rdf and of the spreadsheet
	 * @return
	 */
	private void initMergedPrefixMappings(RepositoryConnection connection){
		mergedPrefixMapping = new HashMap<>();
		
		//prefix mappings of project
		RepositoryResult<Namespace> namespaces = connection.getNamespaces();
		while (namespaces.hasNext()) {
			Namespace ns = namespaces.next();
			mergedPrefixMapping.put(ns.getPrefix(), ns.getName());
		}
		
		//prefix mappings of datasheet (if defined)
		Map<String, String> excelPrefixMapping = spreadsheetManager.getPrefixNamespaceMapping();
		Set<String> prefs = excelPrefixMapping.keySet();
		for (String pref : prefs){
			mergedPrefixMapping.put(pref, excelPrefixMapping.get(pref));
		}
		
		//other useful prefix mappings
		mergedPrefixMapping.put("xsd", XSD.NAMESPACE);
		mergedPrefixMapping.put("coda", ContractConstants.CODA_CONTRACTS_BASE_URI);
	}
	
	/**
	 * Returns the prefix mappings defined in the project, in the datasheet and those useful to S2RDF 
	 * @return
	 */
	public Map<String,String> getMergedPrefixMapping() {
		return this.mergedPrefixMapping;
	}
	
	/**
	 * Generates a pearl file (without skos scheme, in case of OWL scenarios)
	 * @param pearlFile
	 * @throws IOException 
	 */
	public void generatePearlFile(File pearlFile, String sheetName) throws IOException {
		MappingStruct mappingStruct = this.mappingStructMap.get(sheetName);
		PearlGenerator pg = new PearlGenerator(annotationTypeName);
		pg.generatePearl(pearlFile, mappingStruct, mergedPrefixMapping);
	}
	
	/**
	 * Executes the Analysis Engine ExcelAnnotator that populate and return a JCas containing the excel data
	 * @return
	 * @throws UIMAException
	 */
	public JCas executeAnnotator(String sheetName) throws UIMAException, NotExistingSheetException {

		MappingStruct mappingStruct = this.mappingStructMap.get(sheetName);
		S2RDFSheet sheet = this.spreadsheetManager.getSheet(sheetName);

		AnalysisEngine ae;
		if (spreadsheetOrDb.isForSpreadsheetFile()) {
			 ae = AnalysisEngineFactory.createEngine(SheetAnnotator.class,
					SheetAnnotator.EXCEL_FILE, spreadsheetOrDb.getSpreadsheetFile().getAbsolutePath(),
					SheetAnnotator.SHEET_NAME, sheetName,
					SheetAnnotator.ANNOTATION_TYPE_STRING, annotationTypeName,
					SheetAnnotator.CELL_FS_NAME_STRING, cellFSName,
					SheetAnnotator.HEADER_FS_MAP, mappingStruct.convertHeaderFsMapToList());
		} else { // spreadsheetOrDb.isForDb()
			ae = AnalysisEngineFactory.createEngine(DBTableAnnotator.class,
					DBTableAnnotator.DB_BASE_URL, spreadsheetOrDb.getDbInfo().getDb_base_url(),
					DBTableAnnotator.DB_NAME, spreadsheetOrDb.getDbInfo().getDb_name(),
					DBTableAnnotator.DB_TABLE_LIST, spreadsheetOrDb.getDbInfo().getDb_tableList(),
					DBTableAnnotator.DB_TABLE, sheetName,
					DBTableAnnotator.DB_USER, spreadsheetOrDb.getDbInfo().getDb_user(),
					DBTableAnnotator.DB_PASS, spreadsheetOrDb.getDbInfo().getDb_password(),
					DBTableAnnotator.DB_DRIVER_NAME, spreadsheetOrDb.getDbInfo().getDb_driverName().toString(),
					DBTableAnnotator.ANNOTATION_TYPE_STRING, annotationTypeName,
					DBTableAnnotator.CELL_FS_NAME_STRING, cellFSName,
					DBTableAnnotator.HEADER_FS_MAP, mappingStruct.convertHeaderFsMapToList());
		}
		/* Create a useless string of length at least the number of rows of excel file.
		 * It's useful just for the indexing of the annotations in ExcelAnnotator, where every annotation
		 * keep as index begin and end the row index.
		 */
		int nRow = sheet.getRowCount();
		String documentText = "";
		for (int i = 0; i < nRow; i++)
			documentText = documentText + ".";
		// -------------------------------
		TypeSystemDescription tsd = TypeSystemDescriptionCreator.createTypeSystemDescription(
				mappingStruct, annotationTypeName, cellFSName);
		JCas jcas = JCasFactory.createJCas(tsd);
		jcas.setDocumentText(documentText);
		SimplePipeline.runPipeline(jcas, ae);
		
		return jcas;
	}

	public File getSpreadsheetFile() {
		return spreadsheetOrDb.getSpreadsheetFile();
	}

	public SpreadsheetOrDb getSpreadsheetOrDb() {
		return spreadsheetOrDb;
	}
}
